/* Package main provide start environment for many devices

wozultima.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package main

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/color"
	_ "image/png"
	"log"
	"log/slog"
	"os"

	"wozultima/cmd/wozultima/config"
	"wozultima/internal/assets/fonts"
	"wozultima/internal/assets/images"
	"wozultima/internal/workspace"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"github.com/nfnt/resize"
)

const (
	scale          = 1
	tps            = 120
	screenWidth    = 1410
	screenHeight   = 790
	dp             = 300
	normalFontSize = 16
	smallFontSize  = 11
)

const (
	nbLignesTotal    = workspace.NbLignesTotal
	nbLignesEspace12 = workspace.NbLignesEspace12
	nbLignesEspace3  = workspace.NbLignesEspace3
)

type Game struct {
	config *config.Config
}

var (
	fontSource       *text.GoTextFaceSource
	fontSourceNeg    *text.GoTextFaceSource
	couleurs         = []color.RGBA{{0x00, 0xe7, 0x00, 0xff}, {0xff, 0xbd, 0x04, 0xff}, {0xff, 0xff, 0xff, 0xff}} // vert, ambre , blanc
	afficheMessage   = bool(false)
	currentTextColor = int(0)
	workingDirPath   = string("")
	muteSound        = bool(false)
	focus            = int(0)
	ErrTerminated    = errors.New("Terminated")

	messageImage    = bool(false)
	messageImagePng *ebiten.Image
)

func check(e error) {
	if e != nil {
		slog.Error(e.Error())
	}
}

func main() {

	fmt.Println("Start")

	// polices de caractère
	source, err := text.NewGoTextFaceSource(bytes.NewReader(fonts.WozUltimaFont))
	check(err)

	fontSource = source

	source, err = text.NewGoTextFaceSource(bytes.NewReader(fonts.WozUltimaNegFont))
	check(err)

	fontSourceNeg = source

	// espace de travail initialiser
	conf := config.NewConfig()
	check(conf.LoadFromFile())

	game := &Game{
		config: conf,
	}

	currentTextColor = conf.CurrentTextColor
	workingDirPath = conf.WorkingDirPath
	muteSound = conf.MuteSound
	workingDirPath = workspace.CreerEspace(workingDirPath)
	conf.WorkingDirPath = workingDirPath
	check(os.Chdir(workingDirPath))

	ebiten.SetWindowSize(screenWidth*scale, screenHeight*scale)
	ebiten.SetWindowTitle("WozUltima")
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeDisabled)
	ebiten.SetTPS(tps)
	if err := ebiten.RunGameWithOptions(game, &ebiten.RunGameOptions{ScreenTransparent: false}); err != nil {
		if err != ErrTerminated {
			// Irregular termination
			log.Fatalf("Damned ! %v\n", err)
		}

	}
	check(conf.SaveToFile())
	check(os.Chdir(workspace.StartDir))
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight
}

func (g *Game) Draw(screen *ebiten.Image) {
	couleurFonte := couleurs[currentTextColor]
	currentEntry := focus
	lignes := int(0)
	x := float64(20)
	y := float64(normalFontSize)

	if focus > nbLignesEspace12-5 {
		currentEntry = nbLignesEspace12 - 5

	}

	// espace 1
	op := &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)
	texte := workspace.LigneExtract(0, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)

	lignes++

	for lignes < nbLignesEspace12 {
		y += normalFontSize
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		texte := workspace.LigneExtract(0, lignes)

		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		if lignes == currentEntry+2 {
			texte = workspace.LigneExtract(1, lignes)
			op = &text.DrawOptions{}
			op.GeoM.Translate(x+normalFontSize, y)
			op.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSourceNeg,
				Size:   normalFontSize,
			}, op)

		} else {
			texte = workspace.LigneExtract(1, lignes)
			op = &text.DrawOptions{}
			op.GeoM.Translate(x+normalFontSize, y)
			op.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, op)
		}

		lignes++
	}

	// espace 2
	lignes = 0
	x = 708
	y = normalFontSize

	op = &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)

	texte = workspace.LigneExtract(2, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)

	lignes++

	for lignes < nbLignesEspace12 {

		y += normalFontSize
		texte = workspace.LigneExtract(2, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		texte = workspace.LigneExtract(3, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x+normalFontSize, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   smallFontSize,
		}, op)

		lignes++
	}

	// espace 3
	x = float64(20)
	y += normalFontSize
	lignes = 0

	op = &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)

	texte = workspace.LigneExtract(4, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)
	lignes++

	for lignes < nbLignesEspace3 {

		y += normalFontSize
		texte = workspace.LigneExtract(4, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		texte = workspace.LigneExtract(5, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x+normalFontSize, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		lignes++
	}
	//affiche second windows
	if afficheMessage {

		sw, sh := screen.Bounds().Dx(), screen.Bounds().Dy()
		iw, ih := workspace.NbCaracteresEspaceMessage*14, workspace.NbLignesEspaceMessage*normalFontSize
		x = float64(sw)/2 - float64(iw)/2
		y = float64(sh)/2 - float64(ih)/2
		vector.DrawFilledRect(screen, float32(x), float32(y), float32(iw), float32(ih), color.Black, false)
		if messageImage {
			mx := (x + float64(iw)) - float64(messageImagePng.Bounds().Dx())
			op := &ebiten.DrawImageOptions{}
			op.GeoM.Translate(mx-12, y+12)
			screen.DrawImage(messageImagePng, op)
		}
		// espace help

		lignes = 0

		y += 5
		opt := &text.DrawOptions{}
		opt.GeoM.Translate(x, y)
		opt.ColorScale.ScaleWithColor(couleurFonte)

		texte = workspace.LigneExtract(6, lignes)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, opt)
		lignes++

		for lignes < workspace.NbLignesEspaceMessage {

			y += normalFontSize
			texte = workspace.LigneExtract(6, lignes)
			opt = &text.DrawOptions{}
			opt.GeoM.Translate(x, y)
			opt.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, opt)

			texte = workspace.LigneExtract(7, lignes)
			opt = &text.DrawOptions{}
			opt.GeoM.Translate(x+normalFontSize, y+2)
			opt.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, opt)

			lignes++
		}

	}

}

func (g *Game) Update() error {
	err := error(nil)
	_, dy := ebiten.Wheel()
	mx, my := ebiten.CursorPosition()
	if !afficheMessage {

		// move selector (focus)
		if inpututil.IsKeyJustPressed(ebiten.KeyPageUp) {
			focus = 0
			workspace.AfficheEspace(focus)

		}

		if inpututil.IsKeyJustPressed(ebiten.KeyPageDown) {
			focus = workspace.MaxEntries - 1
			workspace.AfficheEspace(focus)

		}

		if inpututil.IsKeyJustPressed(ebiten.KeyDown) || inpututil.IsKeyJustPressed(ebiten.KeyRight) || dy > 0 {
			if focus < workspace.MaxEntries-1 {
				focus++
				workspace.AfficheEspace(focus)
			}

		}

		if inpututil.IsKeyJustPressed(ebiten.KeyUp) || inpututil.IsKeyJustPressed(ebiten.KeyLeft) || dy < 0 {
			if focus > 0 {
				focus--
				workspace.AfficheEspace(focus)
			}
		}

		// Next dir
		if inpututil.IsKeyJustPressed(ebiten.KeyEnter) {
			if workspace.NextEspace(focus) {
				focus = 0
				g.config.WorkingDirPath = workspace.WorkDir.Name
				workspace.AfficheEspace(0)
			}
		}

		// Previous Dir
		if inpututil.IsKeyJustPressed(ebiten.KeyBackspace) {
			if workspace.PreviousEspace() {
				focus = 0
				g.config.WorkingDirPath = workspace.WorkDir.Name
				workspace.AfficheEspace(0)
			}
		}

		// screen color
		if inpututil.IsKeyJustPressed(ebiten.KeyS) {
			currentTextColor++
			if currentTextColor >= len(couleurs) {
				currentTextColor = 0
			}
			g.config.CurrentTextColor = currentTextColor
		}
		// Woz Information
		if inpututil.IsKeyJustPressed(ebiten.KeyI) && !(workspace.WorkDir.EntryIsdir[focus]) {
			if workspace.WorkDir.EntryIsWoz[focus] {
				// Show Woz file informations
				afficheMessage = !afficheMessage
				messageImage = true
				image, _, err := image.Decode(bytes.NewReader(images.WozGenericCoverImage))
				check(err)
				image = resize.Resize(192, 260, image, resize.Lanczos3)
				messageImagePng = ebiten.NewImageFromImage(image)
				workspace.AfficheInfoWoz(focus)
			}
		}

		if inpututil.IsKeyJustPressed(ebiten.KeyE) {
			// Edit Woz file
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyM) {
			// Mute / Unmute Sound
			muteSound = !muteSound
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyV) {
			// Verify Woz file checksum if not exist

		}

		if inpututil.IsKeyJustPressed(ebiten.KeyEscape) {
			// exit program
			err = ErrTerminated
		}

		if inpututil.IsKeyJustPressed(ebiten.KeyH) || inpututil.IsKeyJustPressed(ebiten.KeyF1) {
			// affiche Help
			afficheMessage = !afficheMessage
			messageImage = false

			workspace.AfficheHelp()

		}
		if inpututil.IsKeyJustPressed(ebiten.KeyR) || inpututil.IsKeyJustPressed(ebiten.KeyF5) {
			// Run WozFile with C600G

		}

		if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) || ebiten.IsMouseButtonPressed(ebiten.MouseButton4) {

			if mx > 20 && mx < workspace.NbCaractereEspace12*14 && my > normalFontSize*2 && my < workspace.NbLignesEspace12*normalFontSize {

				nouveauFocus := 0
				for lignes := 0; lignes < workspace.NbLignesEspace12; lignes++ {
					if my > lignes*normalFontSize && my < (lignes+1)*normalFontSize {
						nouveauFocus = lignes - 3
					}

				}
				if nouveauFocus < workspace.MaxEntries {
					if nouveauFocus != focus {
						focus = nouveauFocus
						if focus < 0 {
							focus = 0
						}
						workspace.AfficheEspace(focus)

					}

				}

			}
		}

	} else {
		if pressAnyKey() || ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			afficheMessage = false
		}
	}
	return err
}

func pressAnyKey() bool {
	iskey := bool(false)
	var keys []ebiten.Key
	keys = inpututil.AppendPressedKeys(keys[:0])
	if len(keys) > 0 {
		if inpututil.IsKeyJustPressed(keys[0]) {
			iskey = true
		}

	}
	return iskey
}
