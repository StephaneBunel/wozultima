/* Package config provide start environment

config/config.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License
*/

package config

import (
	"encoding/json"
	"errors"
	"log/slog"
	"os"
	"path/filepath"
)

type Config struct {
	CurrentTextColor int    `json:"currentTextColor"`
	WorkingDirPath   string `json:"workingDirPath"`
	MuteSound        bool   `json:"muteSound"`

	// not public = not exported
	path string
}

func NewConfig() *Config {
	return &Config{}
}

func (c *Config) LoadFromFile(parts ...string) error {
	var err error
	var path string
	var data []byte

	path, err = c.getPath(parts...)
	if err == nil {
		// Register path for later.
		c.path = path

		// Load configuration
		data, err = os.ReadFile(path)
		switch {
		case err == nil:
			err = json.Unmarshal(data, c)
		case errors.Is(err, os.ErrNotExist):
			slog.Info("config: create default:", "path", path)
			err = c.SaveToFile(path)
		}

	}

	return err
}

func (c *Config) SaveToFile(parts ...string) error {
	var err error
	var path string
	var data []byte
	var fh *os.File
	path, err = c.getPath(parts...)
	if err == nil {
		// Save configuration
		err = os.MkdirAll(filepath.Dir(path), 0770)
		if err == nil {
			data, err = json.MarshalIndent(c, "", "  ")
			if err == nil {
				fh, err = os.Create(path)
				if err == nil {

					defer fh.Close()
					_, err = fh.Write(data)
					if err == nil {
						err = fh.Sync()
					}
				}
			}
		}

	}
	return err
}

func (c *Config) getPath(parts ...string) (string, error) {
	var err = error(nil)
	var userConfigDir string
	const (
		defaultConfigDir  = "WozUtlima"
		defaultConfigFile = "config.json"
	)

	// Construct path.
	path := filepath.Join(parts...)
	if path == "" {
		path = c.path
	}
	if path == "" {
		userConfigDir, err = os.UserConfigDir()
		if err == nil {
			path, err = filepath.Abs(filepath.Join(userConfigDir, defaultConfigDir, defaultConfigFile))
		}
	}

	return path, err
}
