/* Package workspace provide workspace managing environment for WozUltima

workspace/workspace.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License
*/

package workspace

import (
	"encoding/hex"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"wozultima/internal/filer"
	"wozultima/internal/woztools"
)

const (
	NbLignesTotal       = 64
	NbLignesEspace12    = int(NbLignesTotal*2/3) - 2
	NbLignesEspace3     = int((NbLignesTotal - NbLignesEspace12) / 3)
	NbCaractereEspace12 = 49
	NbCaractereEspace3  = 99
)

var homeDir filer.Dir
var WorkDir filer.Dir
var currentDir filer.Dir
var err error
var StartDir = string("")

var espace [8][NbLignesTotal]string

var NbLignesEspaceMessage int = 0
var NbCaracteresEspaceMessage int = 0
var MaxEntries int

func check(e error) {
	if e != nil {
		//	log.Fatalf("Something goes wrong %v\n", e)
		slog.Error(e.Error())
	}
}

func CreerEspace(name string) string {
	StartDir, err = os.Getwd()
	check(err)

	homeDirName, err := os.UserHomeDir()
	check(err)
	homeDir = filer.ReadDir(homeDirName)

	WorkDir.Name = homeDir.Name

	if name != "" {

		WorkDir.Name = name
	}

	WorkDir = filer.ReadDir(WorkDir.Name)
	if WorkDir.NbDirs+WorkDir.NbFiles == 0 {
		WorkDir = homeDir
	}

	MaxEntries = WorkDir.NbDirs + WorkDir.NbFiles

	AfficheEspace(0)

	return WorkDir.Name
}
func AfficheEspace(focus int) {
	afficheEspace1(focus)
	afficheEspace2(focus)
	//afficheEspace3(focus)
}

func NextEspace(focus int) bool {
	isNext := bool(false)

	if WorkDir.EntryIsdir[focus] {
		currentDir = filer.ReadDir(WorkDir.Name + "/" + WorkDir.EntryName[focus])
		if currentDir.NbDirs+currentDir.NbFiles != 0 {
			WorkDir = currentDir
			MaxEntries = WorkDir.NbDirs + WorkDir.NbFiles

			err = os.Chdir(WorkDir.Name)
			check(err)
			isNext = true
		}

	}

	return isNext
}

func PreviousEspace() bool {
	isPrevious := bool(false)
	if !(WorkDir.Name == homeDir.Name) {
		err = os.Chdir("..")
		check(err)
		workDirName, err := os.Getwd()
		check(err)
		WorkDir = filer.ReadDir(workDirName)
		MaxEntries = WorkDir.NbDirs + WorkDir.NbFiles
		isPrevious = true
	}

	return isPrevious
}

func afficheEspace1(focus int) {
	currentDir = WorkDir
	currentName := strings.Replace(currentDir.Name, homeDir.Name, "~", 1)
	tailleDir := infoDir(currentDir.GlobalSize, currentDir.NbFiles+currentDir.NbDirs)
	prepareCadre(0, currentName, tailleDir, NbCaractereEspace12, NbLignesEspace12)
	afficheDir(0, focus)

}
func afficheEspace2(focus int) {

	if currentDir.EntryIsdir[focus] {
		currentDir = filer.ReadDir(WorkDir.Name + "/" + currentDir.EntryName[focus])
		currentName := strings.Replace(currentDir.Name, homeDir.Name, "~", 1)
		tailleDir := infoDir(currentDir.GlobalSize, currentDir.NbFiles+currentDir.NbDirs)
		prepareCadre(2, currentName, tailleDir, NbCaractereEspace12, NbLignesEspace12)
		afficheDir(2, focus)

		afficheEspace3(false, 0)

	} else {
		currentName := strings.Replace(currentDir.EntryName[focus], homeDir.Name, "~", 1)
		tailleDir := infoFile(currentDir.EntrySize[focus])
		prepareCadre(2, currentName, tailleDir, NbCaractereEspace12, NbLignesEspace12)
		afficheFichier(2, WorkDir.Name+"/"+currentDir.EntryName[focus])

		afficheEspace3(true, focus)

	}

}

func prepareCadre(numeroEspace int, header string, footer string, width int, height int) {
	lenHeader := strings.Count(header, "") - 1
	lenFooter := strings.Count(footer, "") - 1

	if lenHeader > width-4 {
		header = header[lenHeader-(width-4):]
		lenHeader = width - 4
	}

	if lenFooter > width-4 {
		footer = footer[lenFooter-(width-4):]
		lenFooter = width - 4
	}

	espace[numeroEspace][0] = fmt.Sprintf("%s%s%s%s", "┌─", header, strings.Repeat("─", (width-lenHeader)-4), "─┐")
	espace[numeroEspace+1][0] = ""
	espace[numeroEspace][1] = fmt.Sprintf("%s%s%s", "│", strings.Repeat(" ", width-2), "│")
	espace[numeroEspace+1][1] = ""

	compteurLigne := int(2)
	for compteurLigne < height-2 {
		espace[numeroEspace][compteurLigne] = fmt.Sprintf("%s%s%s", "│", strings.Repeat(" ", width-2), "│")
		espace[numeroEspace+1][compteurLigne] = ""

		compteurLigne++
	}

	espace[numeroEspace][compteurLigne] = fmt.Sprintf("%s%s%s", "│", strings.Repeat(" ", width-2), "│")
	espace[numeroEspace+1][compteurLigne] = ""

	espace[numeroEspace][compteurLigne+1] = fmt.Sprintf("%s%s%s%s", "└─", footer, strings.Repeat("─", (width-lenFooter)-4), "─┘")
	espace[numeroEspace+1][compteurLigne+1] = ""

}

func afficheDir(numeroEspace int, focus int) {
	compteurEntry := int(0)
	compteurLigne := int(0)
	if focus > NbLignesEspace12-5 {
		compteurEntry = focus - (NbLignesEspace12 - 5)
	}

	for compteurLigne < NbLignesEspace12-4 {

		entryInfo := ""
		entryTime := ""
		entryName := ""
		if compteurEntry < len(currentDir.EntryName) {

			entryName = "<" + currentDir.EntryName[compteurEntry] + ">"

			if !currentDir.EntryIsdir[compteurEntry] {

				entryName = currentDir.EntryName[compteurEntry]

			}
			if strings.Count(entryName, "") > NbCaractereEspace12-19 {
				entryName = entryName[0 : NbCaractereEspace12-19]
			}

			entryTime = currentDir.EntryDate[compteurEntry]

		}

		entryInfo = fmt.Sprintf("%s%s%s", entryName, strings.Repeat(" ", NbCaractereEspace12-strings.Count(entryName, "")-17), entryTime)

		espace[numeroEspace][compteurLigne+2] = fmt.Sprintf("%s%s%s", "│", strings.Repeat(" ", NbCaractereEspace12-2), "│")
		espace[numeroEspace+1][compteurLigne+2] = entryInfo

		compteurEntry++
		compteurLigne++

	}

}

func afficheFichier(numeroEspace int, currentFile string) {
	buffer := make([]byte, 16)
	echantillonFile := filer.ReadEchantillonFile(currentFile)

	compteur := int(2)

	for compteur < NbLignesEspace12-2 {

		if len(echantillonFile) > 15 {
			buffer = echantillonFile[0:16]
			echantillonFile = echantillonFile[16 : len(echantillonFile)-16]
		}
		espace[numeroEspace][compteur] = fmt.Sprintf("%s%s%s", "│", strings.Repeat(" ", NbCaractereEspace12-2), "│")

		espace[numeroEspace+1][compteur] = hex.Dump(buffer)[len(hex.Dump(buffer))-69:]
		compteur++

	}

}

func afficheEspace3(isFile bool, focus int) {
	// clean espace3
	information := "Informations : " + currentDir.Name
	if isFile {
		information = "Informations : " + currentDir.EntryName[focus]
	}

	if len(information) > NbCaractereEspace3-4 {
		information = information[len(information)-(NbCaractereEspace3-4):]
	}

	prepareCadre(4, "", information, NbCaractereEspace3, NbLignesEspace3)

	// espace3 content

	espace[5][1] = fmt.Sprintf("%d director(y/ies) and %d files for %d bytes", currentDir.NbDirs, currentDir.NbFiles, currentDir.GlobalSize)

	if isFile {

		espace[5][1] = fmt.Sprintf("1 file for %d bytes", currentDir.EntrySize[focus])

		if currentDir.EntryIsWoz[focus] {
			espace[5][1] = fmt.Sprintf("This is a Woz file version %s for %d bytes with checksum %x ", string(currentDir.EntryWozVersion[focus]), currentDir.EntrySize[focus], currentDir.EntryCrc32Woz[focus])
			if currentDir.EntryCrc32Woz[focus] == 0 {
				espace[5][2] = "This Woz file do not have checksum, checksum is null"
			} else if currentDir.EntryIsOkWoz[focus] {
				espace[5][2] = "This Woz file has been verified, checksum is ok"
			} else {
				espace[5][2] = "This Woz file has not been verified, checksum is mismatch"
			}
		}
	}

}
func AfficheInfoWoz(focus int) {
	NbLignesEspaceMessage = 19
	NbCaracteresEspaceMessage = 60
	woztools.FullReadWozFile(currentDir.EntryName[focus])
	wozdata := int(0)
	prepareCadre(6, " INFO version "+fmt.Sprint(woztools.GetWozInfo8(20)), "", NbCaracteresEspaceMessage, NbLignesEspaceMessage)

	if currentDir.EntryWozVersion[focus] > 49 {
		NbLignesEspaceMessage = 30

		prepareCadre(6, " INFO version "+fmt.Sprint(woztools.GetWozInfo8(20)), "", NbCaracteresEspaceMessage, NbLignesEspaceMessage)
		espace[7][12] = "This disk has " + fmt.Sprint(woztools.GetWozInfo8(57)) + " side(s)"
		espace[7][14] = "Boot sector format "
		wozdata = woztools.GetWozInfo8(58)
		switch wozdata {
		case 1:
			espace[7][14] = espace[7][14] + "has 16-sector"
		case 2:
			espace[7][14] = espace[7][14] + "has 13-sector"
		case 3:
			espace[7][14] = espace[7][14] + "has 16 and 13-sector"
		default:
			espace[7][14] = espace[7][14] + "is unknown"

		}
		espace[7][16] = "Optimal Bit Timing is " + fmt.Sprint(woztools.GetWozInfo8(59)*128/1000) + " microsecond(s)"

		wozdata16 := woztools.GetWozInfo16(62)

		switch {
		case wozdata16 == 0:
			espace[7][18] = "Required RAM is unknown"

		default:

			espace[7][18] = "Required Ram is " + fmt.Sprint(wozdata16) + " KB"

		}

		espace[7][20] = "The largest track uses " + fmt.Sprint(woztools.GetWozInfo16(64)) + " block(s)"

		if woztools.GetWozInfo16(66) != 0 && woztools.GetWozInfo16(68) == 0 {
			espace[7][22] = "FLUX and Largest Flux Track blocks are incoherent"
		} else if woztools.GetWozInfo16(66) == 0 && woztools.GetWozInfo16(68) == 0 {
			espace[7][22] = "FLUX and Largest Flux Track blocks are unknown"
		} else {
			espace[7][22] = "Flux has " + fmt.Sprint(woztools.GetWozInfo16(66)) + " and Largest track has " + fmt.Sprint(woztools.GetWozInfo16(68)) + " block(s)"
		}
		w := (NbCaracteresEspaceMessage - 19) / 2
		espace[7][24] = strings.Repeat(" ", w) + "Compatible Hardware" + strings.Repeat(" ", w)
		espace[7][25] = "☐ Apple II ☐ Apple II Plus ☐ Apple //e"
		espace[7][26] = "☐ Apple //e Enhanced ☐ Apple //c ☐ Apple //c Plus"
		espace[7][27] = "☐ Apple IIgs ☐ Apple /// ☐ Apple /// Plus"
		wozdata16 = woztools.GetWozInfo16(60)

		if wozdata16 == 0 {
			w = (NbCaracteresEspaceMessage - 38) / 2
			espace[7][24] = ""
			espace[7][25] = strings.Repeat(" ", w) + "Hardware compatibility list is unkown" + strings.Repeat(" ", w)
			espace[7][26] = ""
			espace[7][27] = ""
			espace[7][28] = ""
		} else {
			w = (NbCaracteresEspaceMessage - 40) / 2
			if wozdata16&1 == 1 { // Apple II
				espace[7][25] = replaceEmptyBox(espace[7][25], 1)
			}

			if wozdata16&2 == 2 { // Apple II Plus
				espace[7][25] = replaceEmptyBox(espace[7][25], 12)
			}
			if wozdata16&4 == 4 { // Apple IIe+
				espace[7][25] = replaceEmptyBox(espace[7][25], 28)
			}
			espace[7][25] = strings.Repeat(" ", w) + espace[7][25] + strings.Repeat(" ", w)
			w = (NbCaracteresEspaceMessage - 50) / 2
			if wozdata16&10 == 10 { // Apple //e Enhanced
				espace[7][26] = replaceEmptyBox(espace[7][26], 1)
			}
			if wozdata16&8 == 8 { // Apple //c
				espace[7][26] = replaceEmptyBox(espace[7][26], 22)
			}
			if wozdata16&40 == 40 { // Apple //c Plus
				espace[7][26] = replaceEmptyBox(espace[7][26], 34)
			}
			espace[7][26] = strings.Repeat(" ", w) + espace[7][26] + strings.Repeat(" ", w)
			w = (NbCaracteresEspaceMessage - 42) / 2
			if wozdata16&20 == 20 { // Apple IIgs
				espace[7][27] = replaceEmptyBox(espace[7][27], 1)
			}
			if wozdata16&80 == 80 { // Apple ///
				espace[7][27] = replaceEmptyBox(espace[7][27], 14)
			}
			if wozdata16&100 == 100 { // Apple /// Plus
				espace[7][27] = replaceEmptyBox(espace[7][27], 26)
			}
			espace[7][27] = strings.Repeat(" ", w) + espace[7][27] + strings.Repeat(" ", w)
		}

	}

	espace[7][2] = "Creator is " + woztools.GetWozInfoTexte(25, 32)
	espace[7][4] = "Woz file type disk is a "

	wozdata = woztools.GetWozInfo8(21)

	switch wozdata {
	case 1:
		espace[7][4] = espace[7][4] + "5\" 1/4"
	case 2:
		espace[7][4] = espace[7][4] + "3\" 1/2"
	default:
		espace[7][4] = espace[7][4] + "unknown format"

	}

	espace[7][6] = "The disk is "
	wozdata = woztools.GetWozInfo8(22)
	if wozdata == 1 {
		espace[7][6] = espace[7][6] + "write protected"
	} else {
		espace[7][6] = espace[7][6] + "not write protected"
	}
	espace[7][8] = "Cross track sync was "

	wozdata = woztools.GetWozInfo8(23)
	if wozdata == 1 {
		espace[7][8] = espace[7][8] + "used during imaging"
	} else {
		espace[7][8] = espace[7][8] + "not used during imaging"
	}

	espace[7][10] = "MC3470 fake bits have "
	wozdata = woztools.GetWozInfo8(24)
	if wozdata == 1 {
		espace[7][10] = espace[7][10] + "been removed"
	} else {
		espace[7][10] = espace[7][10] + "not been removed"
	}

}

func replaceEmptyBox(originaltext string, index int) string {
	runesText := []rune(originaltext)
	partOne := string(runesText[0 : index-1])
	partTwo := string(runesText[index:])
	return partOne + "☑" + partTwo
}

func AfficheHelp() {
	NbLignesEspaceMessage = 19
	NbCaracteresEspaceMessage = 50
	prepareCadre(6, " HELP ", "", NbCaracteresEspaceMessage, NbLignesEspaceMessage)
	idx := 1
	espace[7][idx] = "<H> or <F1> Help"
	idx++
	espace[7][idx] = "<I> Informations about selected Woz file"
	idx++
	espace[7][idx] = "<V> Verify selected Woz file"
	idx++
	espace[7][idx] = "<ESC> Escape WozUltima"
	idx++
	espace[7][idx] = "<↓> or <←> Down selection entries"
	idx++
	espace[7][idx] = "<→> or <↑> Up selection entries"
	idx++
	espace[7][idx] = "<⇓> Last selection entries"
	idx++
	espace[7][idx] = "<⇑> First selection entries"
	idx++
	espace[7][idx] = "<C> Convert Woz file(s) to lastest version"
	idx++
	espace[7][idx] = "<E> Edit Woz file"
	idx++
	espace[7][idx] = "<S> Screen font color"
	idx++
	espace[7][idx] = "<M> Mute / Unmute sound"
	idx++
	espace[7][idx] = "<SpaceBar> Select/Unselect file"
	idx++
	espace[7][idx] = "<Enter> Enter in selected directory"
	idx++
	espace[7][idx] = "<BackSpace> Return previous directory"
	idx++
	espace[7][idx] = "<R> or <F5> Run Woz file with C600G"
	idx++
	espace[7][idx] = strings.Repeat(" ", 6) + "Mouse buttons and wheel for selection" + strings.Repeat(" ", 6)
}

func LigneExtract(numeroEspace int, numeroLigne int) string {

	return espace[numeroEspace][numeroLigne]

}

func infoDir(somme int64, nb_entry int) string {
	var resultat string

	if somme > 1073741824 {
		resultat = fmt.Sprintf(" %d entrie(s) for %d GB ", nb_entry, somme/(1<<30))
	} else if somme > 1048576 {
		resultat = fmt.Sprintf(" %d entrie(s) for %d MB ", nb_entry, somme/(1<<20))
	} else {
		resultat = fmt.Sprintf(" %d entrie(s) for %d KB ", nb_entry, somme/(1<<10))
	}

	return resultat

}

func infoFile(sizeFile int64) string {
	var resultat string

	if sizeFile > 1073741824 {
		resultat = fmt.Sprintf(" %d GB ", sizeFile/(1<<30))
	} else if sizeFile > 1048576 {
		resultat = fmt.Sprintf(" %d MB ", sizeFile/(1<<20))
	} else {
		resultat = fmt.Sprintf(" %d KB ", sizeFile/(1<<10))
	}

	return resultat

}
