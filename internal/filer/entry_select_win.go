//go:build windows

/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"io/fs"
	"log/slog"
	"os"
	"syscall"
)

func selectEntry(path string, info fs.FileInfo) bool {
	isSelect := bool(true)
	name := info.Name()
	mode := info.Mode()

	pointer, err := syscall.UTF16PtrFromString(path)
	check(err)
	attributes, err := syscall.GetFileAttributes(pointer)
	check(err)
	isSelect = attributes&syscall.FILE_ATTRIBUTE_HIDDEN == 0

	if name[0] == '.' || name[len(name)-1] == '~' || mode&os.ModeSymlink != 0 {
		isSelect = false
	}

	// a revoir
	fh, err := os.Open(path)
	if err != nil {
		isSelect = false
	} else if err := fh.Close(); err != nil {
		slog.Error(err.Error())
	}
	return isSelect

}
