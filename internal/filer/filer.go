/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"bufio"
	"io"
	"log/slog"
	"os"
	"path/filepath"
	"wozultima/internal/woztools"
)

type sortType string

const (
	sortName     sortType = "Name[^]"
	sortNameRev  sortType = "Name[$]"
	sortSize     sortType = "Size[^]"
	sortSizeRev  sortType = "Size[$]"
	sortMtime    sortType = "Time[^]"
	sortMtimeRev sortType = "Time[$]"
)

type Dir struct {
	Name            string
	NbDirs          int
	NbFiles         int
	GlobalSize      int64
	EntryName       []string
	EntryIsdir      []bool
	EntryIsWoz      []bool
	EntryCrc32Woz   []uint32
	EntryIsOkWoz    []bool
	EntryWozVersion []uint8
	EntrySize       []int64
	EntryDate       []string
	Sort            sortType
}

func check(e error) {
	if e != nil {
		if e != io.EOF {
			slog.Error(e.Error())
		}
	}
}

func ReadDir(name string) (d Dir) {
	var dir Dir
	entries, err := os.ReadDir(name)
	check(err)
	dir.Name = name
	dir.GlobalSize = 0
	dir.NbDirs = 0
	dir.NbFiles = 0
	dir.Sort = sortName

	compteur := 0
	for _, entry := range entries {

		info, err := entry.Info()
		check(err)

		if selectEntry(filepath.Join(name, info.Name()), info) {
			dir.EntryName = append(dir.EntryName, info.Name())
			dir.EntryDate = append(dir.EntryDate, info.ModTime().Format("2006-01-02 15:04"))
			dir.EntrySize = append(dir.EntrySize, 0)
			dir.EntryIsdir = append(dir.EntryIsdir, true)
			dir.EntryIsWoz = append(dir.EntryIsWoz, false)
			dir.EntryWozVersion = append(dir.EntryWozVersion, 0)
			dir.EntryIsOkWoz = append(dir.EntryIsOkWoz, false)
			dir.EntryCrc32Woz = append(dir.EntryCrc32Woz, 0)
			if entry.IsDir() {
				dir.NbDirs++
			} else {
				dir.GlobalSize += info.Size()
				dir.NbFiles++
				dir.EntrySize[compteur] = info.Size()
				dir.EntryIsdir[compteur] = false

				dir.EntryIsWoz[compteur] = woztools.IsWozFile(filepath.Join(name, dir.EntryName[compteur]))
				if dir.EntryIsWoz[compteur] {
					dir.EntryWozVersion[compteur], dir.EntryIsOkWoz[compteur], dir.EntryCrc32Woz[compteur] = woztools.CheckWoz(filepath.Join(name, dir.EntryName[compteur]))

				}
			}
			compteur++
		}
	}
	return dir
}

func ReadEchantillonFile(name string) []byte {
	file, err := os.Open(name)
	check(err)
	defer file.Close()

	reader := bufio.NewReader(file)
	buf := make([]byte, 1152)

	_, err = reader.Read(buf)
	check(err)

	file.Close()

	return buf
}
