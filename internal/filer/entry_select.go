//go:build !windows

/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"io/fs"
	"log/slog"
	"os"
)

func selectEntry(path string, info fs.FileInfo) bool {
	isSelect := true
	name := info.Name()
	mode := info.Mode()

	if name[0] == '.' || name[len(name)-1] == '~' || mode&os.ModeSymlink != 0 {
		isSelect = false
	}

	fh, err := os.Open(path)
	if err != nil {
		isSelect = false
	} else if err := fh.Close(); err != nil {
		slog.Error(err.Error())
	}

	return isSelect
}
