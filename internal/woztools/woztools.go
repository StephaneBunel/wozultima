/* Package woz provide woz format functions

woztools/woztools.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package woztools

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"fmt"
	"hash/crc32"
	"io"
	"log/slog"
	"os"
)

type WozFile struct {
	name    string
	crc32   uint32
	version uint8
	info    []byte
	data    []byte
}

var AwozFile WozFile

var (
	compatibilityHardware = map[uint16]string{
		0x0001: "Apple ][",
		0x0002: "Apple ][ Plus",
		0x0004: "Apple //e",
		0x0008: "Apple //c",
		0x0010: "Apple //e Enhanced",
		0x0020: "Apple IIgs",
		0x0040: "Apple //c Plus",
		0x0080: "Apple ///",
		0x0100: "Apple /// Plus"}
)

func check(e error) {
	if e != nil {
		if e != io.EOF {
			slog.Error(e.Error())

		}
	}
}

func CheckWoz(path string) (uint8, bool, uint32) {
	crcOk := false
	crc32Woz := uint32(0)
	version := uint8(0)
	if IsWozFile(path) {
		FullReadWozFile(path)
		version = AwozFile.version
		crc32Woz = AwozFile.crc32
		if crc32Woz == crc32.ChecksumIEEE(AwozFile.data) {
			crcOk = true
		}

	}
	return version, crcOk, crc32Woz
}

func FullReadWozFile(path string) {
	file, err := os.ReadFile(path)
	check(err)
	AwozFile.name = path
	AwozFile.version = uint8(file[3])
	AwozFile.info = file[0:70]
	AwozFile.data = file[12:]
	AwozFile.crc32 = binary.LittleEndian.Uint32(file[8:12])

}

func readPartialWozFile(path string, buffer byte) []byte {
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	reader := bufio.NewReader(file)
	buf := make([]byte, buffer)

	_, err = reader.Read(buf)
	check(err)

	file.Close()

	return buf
}

func IsWozFile(path string) bool {
	isWoz := false
	entete := readPartialWozFile(path, 70)

	if bytes.Equal(entete[0:3], []byte{0x57, 0x4f, 0x5a}) {
		if entete[4] == 0xff && bytes.Equal(entete[5:8], []byte{0x0A, 0x0D, 0x0A}) && bytes.Equal(entete[12:16], []byte{0x49, 0x4e, 0x46, 0x4f}) {
			isWoz = true

		}
	}

	return isWoz
}

func GetWozEntry8(offset int) string {

	return string(AwozFile.data[offset : offset+1][0])

}

func GetWozInfoUint8(offset int) uint8 {

	return uint8(AwozFile.data[offset : offset+1][0])

}

func GetWozEntry16(offset int) string {

	return fmt.Sprintf("%x", binary.LittleEndian.Uint16(AwozFile.data[offset:offset+2]))

}

func GetWozEntry32(offset int) string {

	return fmt.Sprintf("%x", binary.LittleEndian.Uint32(AwozFile.data[offset:offset+4]))

}

func GetWozInfo8(offset int) int {

	return int(AwozFile.info[offset])

}

func GetWozInfo16(offset int) uint16 {

	return binary.LittleEndian.Uint16(AwozFile.info[offset : offset+2])

}

func GetWozInfo32(offset int) uint32 {

	return binary.LittleEndian.Uint32(AwozFile.info[offset : offset+4])

}

func GetWozInfoTexte(offset int, long int) string {
	return string(AwozFile.info[offset : offset+long])

}
