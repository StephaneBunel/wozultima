# Intro

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

## Feature overview

- [x] **Easy to use** like an graphical app
- [x] **Feature overview and Contents** for fast orientation
- [x] **Identify and verify Woz file**
- [x] **Get Informations of Woz file**
- [ ] **Copy,rename and move Woz files (bulk opérations)**
- [ ] **Update in latest Woz Format**
- [ ] **Edit and analyse Woz File**
- [ ] **Run Woz File with C600G an accurate APPLE IIe emulator**

## Contents

- [What is this?](#what-is-this)
- [When should I use this?](#when-should-i-use-this)
- [Getting started](#getting-started)
  - [Requirements](#requirements)
  - [Install](#install)
  - [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)
- [Conclusion](#conclusion)
- [Sources](#Sources)

## What is this?

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files

## Why should I use this?

If you want manage, check, update your Woz Files Library or help others libraries, you may use WozUltima

## Getting Started

This program is writting in Golang with Ebiten Game engine for graphical and sound elements (emulator) is
compatible with plateforms :
*   [x] **Gnu/Linux, FreeBSD and Steam Deck**
*   [x] **MacOS and Windows**
*   [ ] **Android and Ios**
*   [ ] **Nintendo Switch**

### Requirements for build

- Need GoLang version 1.22.3 or newer
- Basic knowledge of Apple II specifications and Disk II spécifications

### Install

```
$ git clone https://gitlab.com/aldo.reset/wozultima
$ cd wozultima
$ go mod tidy
```

### Usage

```
$ make run
```

OR if already compiled

```
$ ./bin/wozultima
```

- [x]\<H> or \<F1> Help
- [x]\<I> Informations about selected Woz file
- [ ]\<V> Verify selected Woz file
- [x]\<ESC> Escape WozUltima (Quit)
- [x]\<↓> or <←> Down selection entries
- [x]\<→> or <↑> Up selection entries
- [x]\<⇓> Last selection entries
- [x]\<⇑> First selection entries
- [ ]\<C> Convert Woz file(s) to lastest version
- [ ]\<E> Edit Woz file
- [x]\<M> Mute / Unmute sound
- [x]\<S> Screen font color
- [ ]\<SpaceBar> Select/Unselect file
- [x]\<Enter> Enter in selected directory
- [x]\<BackSpace> Return previous directory
- [ ]\<R> or \<F5> Run Woz file with C600G
- [*]Mouse button and wheel for selection

## Contribute

Under the same license conditions, Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GNU Affero General Public License](http://www.fsf.org/licensing/licenses/agpl-3.0.html)

## Conclusion

To summarize.. Enjoy !

Please, you want help for developpment, give a [tips](https://cwallet.com/t/6SPJ1BCJ)
Thank you a lot !

## Sources

[Woz 1.0 Specifications](https://applesaucefdc.com/woz/reference1/)
[Woz 2.1 Specifications](https://applesaucefdc.com/woz/reference2/)
