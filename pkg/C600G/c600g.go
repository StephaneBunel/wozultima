/*Package c600g provide Apple IIe emulator to play Woz format file

  c600G.go

  C600G is an accurate Apple IIe emulator writting in Golang
  (C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package c600g
