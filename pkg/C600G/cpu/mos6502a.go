/* Package wozultima/C600G/cpu provide  m6502a Cpu emulator cycle accurate

	mos6502a.go

	C600G is an accurate Apple IIe emulator writting in Golang
  	WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
  	(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License
*/

package cpu

import (
	"fmt"
	"log/slog"
	"time"
	"wozultima/pkg/C600G/cpu/bus"
)

var computerBus bus.Bus

/* address bus pins */
const m6502A0 = uint64(1 << 0)
const m6502A1 = uint64(1 << 1)
const m6502A2 = uint64(1 << 2)
const m6502A3 = uint64(1 << 3)
const m6502A4 = uint64(1 << 4)
const m6502A5 = uint64(1 << 5)
const m6502A6 = uint64(1 << 6)
const m6502A7 = uint64(1 << 7)
const m6502A8 = uint64(1 << 8)
const m6502A9 = uint64(1 << 9)
const m6502A10 = uint64(1 << 10)
const m6502A11 = uint64(1 << 11)
const m6502A12 = uint64(1 << 12)
const m6502A13 = uint64(1 << 13)
const m6502A14 = uint64(1 << 14)
const m6502A15 = uint64(1 << 15)

/* data bus pins */
const m6502D0 = uint64(1 << 16)
const m6502D1 = uint64(1 << 17)
const m6502D2 = uint64(1 << 18)
const m6502D3 = uint64(1 << 19)
const m6502D4 = uint64(1 << 20)
const m6502D5 = uint64(1 << 21)
const m6502D6 = uint64(1 << 22)
const m6502D7 = uint64(1 << 23)

/* control pins */
const m6502RW = uint64(1 << 24)   /* out: memory read or write access */
const m6502SYNC = uint64(1 << 25) /* out: start of a new instruction */
const m6502IRQ = uint64(1 << 26)  /* in: maskable interrupt requested */
const m6502NMI = uint64(1 << 27)  /* in: non-maskable interrupt requested */
const m6502RDY = uint64(1 << 28)  /* in: freeze execution at next read cycle */
const m6510AEC = uint64(1 << 29)  /* in, m6510 only, put bus lines into tristate mode, not implemented */
const m6502RES = uint64(1 << 30)  /* request RESET */

/* m6510 IO port pins */
const m6510P0 = uint64(1 << 32)
const m6510P1 = uint64(1 << 33)
const m6510P2 = uint64(1 << 34)
const m6510P3 = uint64(1 << 35)
const m6510P4 = uint64(1 << 36)
const m6510P5 = uint64(1 << 37)
const m6510PORTBITS = m6510P0 | m6510P1 | m6510P2 | m6510P3 | m6510P4 | m6510P5

/* bit mask for all CPU pins = (up to bit pos 40) */
const m6502PINMASK = uint64((1 << 40) - 1)

/* CPU state */
type M6502a struct {
	IR         uint16
	PC         uint16
	AD         uint16
	A          uint8
	X          uint8
	Y          uint8
	S          uint8
	P          uint8
	brkflags   uint8
	bcdenabled bool
	Pins       uint64
	irqpip     uint16
	nmipip     uint16
	// iopullup   uint8
}

/* status indicator flags */
const m6502CF = uint8(1 << 0) /* carry */
const m6502ZF = uint8(1 << 1) /* zero */
const m6502IF = uint8(1 << 2) /* IRQ disable */
const m6502DF = uint8(1 << 3) /* decimal mode */
const m6502BF = uint8(1 << 4) /* BRK command */
const m6502XF = uint8(1 << 5) /* unused */
const m6502VF = uint8(1 << 6) /* overflow */
const m6502NF = uint8(1 << 7) /* negative */

/* internal BRK state flags */
const m6502BRKIRQ = (1 << 0)   /* IRQ was triggered */
const m6502BRKNMI = (1 << 1)   /* NMI was triggered */
const m6502BRKRESET = (1 << 2) /* RES was triggered */

const resetVector = 0xFFFC
const cpuFrequency = time.Duration(1027)

func check(e error) {
	if e != nil {

		slog.Error(e.Error())

	}
}

func (cpu *M6502a) M6502aInit(bcddisabled bool) time.Duration {
	computerBus, _ = bus.NewBus()
	// specified, https://www.pagetable.com/?p=410
	cpu.P = 0x34
	cpu.A = 0xAA
	cpu.X = 0x00
	cpu.Y = 0x00
	cpu.S = 0xFD
	cpu.PC = computerBus.Read16(resetVector)
	cpu.bcdenabled = !bcddisabled
	//cpu.iopullup = desc.iopullup
	// cpu.Pins

	return cpuFrequency
}

var cycles int64

func (cpu *M6502a) CpuStep() {

	fmt.Println("Cpu Step, World! ", cycles)
	cycles++
}

func (cpu *M6502a) m6502step() {
	cpu.Pins = cpu.m6502tick(cpu.Pins)
	// extract 16-bit address from pin mask
	addr := cpu.m6502GETADDR(cpu.Pins)
	// perform memory access
	if (cpu.Pins & m6502RW) != 0 {
		// a memory read
		cpu.m6502SETDATA(cpu.Pins, computerBus.Read(addr))
	} else {
		// a memory write
		computerBus.Write(addr, cpu.m6502GETDATA(cpu.Pins))
	}

}

/* extract 16-bit address bus from 64-bit pins */
func (cpu *M6502a) m6502GETADDR(pins uint64) uint16 { return uint16(pins & 0xFFFF) }

/* merge 16-bit address bus value into 64-bit pins */
func (cpu *M6502a) m6502SETADDR(pins uint64, addr uint16) {
	cpu.Pins = (pins & 0xFFFF0000) | uint64(addr&0xFFFF)
}

/* extract 8-bit data bus from 64-bit pins */
func (cpu *M6502a) m6502GETDATA(pins uint64) uint8 { return uint8((pins & 0xFF0000) >> 16) }

/* merge 8-bit data bus value into 64-bit pins */
func (cpu *M6502a) m6502SETDATA(pins uint64, data uint8) {
	cpu.Pins = (pins & 0xFF00FFFF) | ((uint64(data) << 16) & 0xFF0000)
}

/* copy data bus value from other pin mask */
func (cpu *M6502a) m6502COPYDATA(pinsDst uint64, pinsSrc uint64) uint64 {
	return (pinsDst & 0xFF00FFFF) | (pinsSrc & 0xFF0000)
}

/* return a pin mask with control-pins, address and data bus */
func (cpu *M6502a) m6502MAKEPINS(ctrl uint64, addr uint16, data uint8) uint64 {
	return ctrl | ((uint64(data) << 16) & 0xFF0000) | uint64(addr&0xFFFF)
}

/* set the port bits on the 64-bit pin mask */
func (cpu *M6502a) m6510SETPORT(pins uint64, data uint8) {
	cpu.Pins = (pins & m6510PORTBITS) | ((uint64(data) << 32) & m6510PORTBITS)
}

/* m6510: check for IO port access to address 0 or 1 */
func (cpu *M6502a) m6510CHECKIO(pins uint64) bool { return pins&0xFFFE == 0 }

/* set 16-bit address in 64-bit pin mask */
func (cpu *M6502a) SA(addr uint16) { cpu.Pins = (cpu.Pins & 0xFFFF0000) | uint64(addr) }

/* extract 16-bit addess from pin mask */
func (cpu *M6502a) GA() uint16 { return uint16(cpu.Pins & 0xFFFF) }

/* set 16-bit address and 8-bit data in 64-bit pin mask */
func (cpu *M6502a) SAD(addr uint16, data uint8) {
	cpu.Pins = (cpu.Pins & 0xFF000000) | (uint64(data) << 16) | uint64(addr)
}

/* fetch next opcode byte */
func (cpu *M6502a) FETCH() {
	cpu.SA(cpu.PC)
	cpu.ON(m6502SYNC)
}

/* set 8-bit data in 64-bit pin mask */
func (cpu *M6502a) SD(data uint8) { cpu.Pins = (cpu.Pins & 0xFF00FFFF) | uint64(data)<<16 }

/* extract 8-bit data from 64-bit pin mask */
func (cpu *M6502a) GD() uint8 { return uint8((cpu.Pins & 0xFF0000) >> 16) }

/* enable control pins */
func (cpu *M6502a) ON(m uint64) { cpu.Pins |= m }

/* disable control pins */
func (cpu *M6502a) OFF(m uint64) { cpu.Pins &= m ^ 0xFFFFFFFF }

/* a memory read tick */
func (cpu *M6502a) RD() { cpu.ON(m6502RW) }

/* a memory write tick */
func (cpu *M6502a) WR() { cpu.OFF(m6502RW) }

func (cpu *M6502a) m6502tick(pins uint64) uint64 {
	if pins&(m6502SYNC|m6502IRQ|m6502NMI|m6502RDY|m6502RES) != 0 {
		// interrupt detection also works in RDY phases, but only NMI is "sticky"

		if 0 != ((pins & (pins ^ cpu.Pins)) & m6502NMI) {
			cpu.nmipip |= 1
		}
		// IRQ test is level triggered
		if (pins&m6502IRQ != 0) && (0 == (cpu.P & m6502IF)) {
			cpu.irqpip |= 1
		}

		if pins&m6502SYNC != 0 {
			// load new instruction into 'instruction register' and restart tick counter
			cpu.IR = uint16(cpu.GD()) << 3
			cpu.OFF(m6502SYNC)

			// check IRQ, NMI and RES state
			//  - IRQ is level-triggered and must be active in the full cycle
			//    before SYNC
			//  - NMI is edge-triggered, and the change must have happened in
			//    any cycle before SYNC
			//  - RES behaves slightly different than on a real 6502, we go
			//    into RES state as soon as the pin goes active, from there
			//    on, behaviour is 'standard'

			// if interrupt or reset was requested, force a BRK instruction
			if cpu.brkflags != 0 {
				cpu.IR = 0x0000
				cpu.P &= m6502BF ^ 0xFF
				pins &= m6502RES ^ 0xFFFFFFF
			} else {
				cpu.PC++
			}
		}
	}
	// reads are default, writes are special
	cpu.RD()

	switch cpu.IR {
	/* BRK  */
	case (0x00 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x00 << 3) | 1:
		if 0 == (cpu.brkflags & (m6502BRKIRQ | m6502BRKNMI)) {
			cpu.PC++
		}
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), uint8(cpu.PC>>8))
		if 0 == (cpu.brkflags & m6502BRKRESET) {
			cpu.WR()
		}
	case (0x00 << 3) | 2:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), uint8(cpu.PC))
		if 0 == (cpu.brkflags & m6502BRKRESET) {
			cpu.WR()
		}
	case (0x00 << 3) | 3:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), uint8(cpu.P|m6502XF))
		if 0 != cpu.brkflags&m6502BRKRESET {
			cpu.AD = 0xFFFC
		} else {
			cpu.WR()
			if cpu.brkflags&m6502BRKNMI != 0 {
				cpu.AD = 0xFFFA
			} else {
				cpu.AD = 0xFFFE
			}
		}
	case (0x00 << 3) | 4:
		cpu.AD++
		cpu.SA(cpu.AD)
		cpu.P |= (m6502IF | m6502BF)
		cpu.brkflags = 0 /* RES/NMI hijacking */
	case (0x00 << 3) | 5:
		cpu.SA(cpu.AD)
		cpu.AD = uint16(cpu.GD()) /* NMI "half-hijacking" not possible */
	case (0x00 << 3) | 6:
		cpu.PC = uint16(cpu.GD())<<8 | cpu.AD
		cpu.FETCH()
	/* ORA (zp,X) */
	case (0x01 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x01 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x01 << 3) | 2:
		cpu.AD += uint16(cpu.X) & 0xFF
		cpu.SA(cpu.AD)
	case (0x01 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x01 << 3) | 4:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x01 << 3) | 5:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x02 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x02 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* SLO (zp,X) (undoc) */
	case (0x03 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x03 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x03 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x03 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x03 << 3) | 4:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x03 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x03 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x03 << 3) | 7:
		cpu.FETCH()
		/* NOP zp (undoc) */
	case (0x04 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x04 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x04 << 3) | 2:
		cpu.FETCH()
	/* ORA zp */
	case (0x05 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x05 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x05 << 3) | 2:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ASL zp */
	case (0x06 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x06 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x06 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x06 << 3) | 3:
		cpu.SD(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x06 << 3) | 4:
		cpu.FETCH()
	/* SLO zp (undoc) */
	case (0x07 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x07 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x07 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x07 << 3) | 3:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x07 << 3) | 4:
		cpu.FETCH()
	/* PHP  */
	case (0x08 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x08 << 3) | 1:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), cpu.P|m6502XF)
		cpu.WR()
	case (0x08 << 3) | 2:
		cpu.FETCH()
	/* ORA # */
	case (0x09 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x09 << 3) | 1:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ASL A  */
	case (0x0A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x0A << 3) | 1:
		cpu.A = cpu.m6502shiftleft(0, cpu.A)
		cpu.FETCH()
	/* ANC # (undoc) */
	case (0x0B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x0B << 3) | 1:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		if cpu.A&0x80 != 0 {
			cpu.P |= m6502CF
		} else {
			cpu.P &= m6502CF ^ 0xFF
		}
		cpu.FETCH()
	/* NOP abs (undoc) */
	case (0x0C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x0C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x0C << 3) | 2:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x0C << 3) | 3:
		cpu.FETCH()
	/* ORA abs */
	case (0x0D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x0D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x0D << 3) | 2:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x0D << 3) | 3:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ASL abs */
	case (0x0E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x0E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x0E << 3) | 2:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x0E << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x0E << 3) | 4:
		cpu.SD(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x0E << 3) | 5:
		cpu.FETCH()
	/* SLO abs (undoc) */
	case (0x0F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x0F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x0F << 3) | 2:
		cpu.SA(uint16(cpu.GD())<<8 | cpu.AD)
	case (0x0F << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x0F << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x0F << 3) | 5:
		cpu.FETCH()
	/* BPL # */
	case (0x10 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x10 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x80) != 0x0 {
			cpu.FETCH()
		}
	case (0x10 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0x10 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* ORA (zp),Y */
	case (0x11 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x11 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x11 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x11 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x11 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x11 << 3) | 5:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x12 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x12 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* SLO (zp),Y (undoc) */
	case (0x13 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x13 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x13 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x13 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x13 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x13 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x13 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x13 << 3) | 7:
		cpu.FETCH()
	/* NOP zp,X (undoc) */
	case (0x14 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x14 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x14 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x14 << 3) | 3:
		cpu.FETCH()
	/* ORA zp,X */
	case (0x15 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x15 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x15 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x15 << 3) | 3:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ASL zp,X */
	case (0x16 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x16 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x16 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x16 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x16 << 3) | 4:
		cpu.SD(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x16 << 3) | 5:
		cpu.FETCH()
	/* SLO zp,X (undoc) */
	case (0x17 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x17 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x17 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x17 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x17 << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x17 << 3) | 5:
		cpu.FETCH()
	/* CLC  */
	case (0x18 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x18 << 3) | 1:
		cpu.P &= 0xFE
		cpu.FETCH()
	/* ORA abs,Y */
	case (0x19 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x19 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x19 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x19 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x19 << 3) | 4:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0x1A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x1A << 3) | 1:
		cpu.FETCH()
	/* SLO abs,Y (undoc) */
	case (0x1B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x1B << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x1B << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x1B << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x1B << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x1B << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x1B << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0x1C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x1C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x1C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x1C << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x1C << 3) | 4:
		cpu.FETCH()
	/* ORA abs,X */
	case (0x1D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x1D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x1D << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x1D << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x1D << 3) | 4:
		cpu.A |= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ASL abs,X */
	case (0x1E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x1E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x1E << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x1E << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x1E << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x1E << 3) | 5:
		cpu.SD(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x1E << 3) | 6:
		cpu.FETCH()
	/* SLO abs,X (undoc) */
	case (0x1F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x1F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x1F << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x1F << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x1F << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x1F << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftleft(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A |= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x1F << 3) | 6:
		cpu.FETCH()
	/* JSR  */
	case (0x20 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x20 << 3) | 1:
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.AD = uint16(cpu.GD())
	case (0x20 << 3) | 2:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), uint8(cpu.PC>>8))
		cpu.WR()
	case (0x20 << 3) | 3:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), uint8(cpu.PC))
		cpu.WR()
	case (0x20 << 3) | 4:
		cpu.SA(cpu.PC)
	case (0x20 << 3) | 5:
		cpu.PC = (uint16(cpu.GD()) << 8) | cpu.AD
		cpu.FETCH()
	/* AND (zp,X) */
	case (0x21 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x21 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x21 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x21 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x21 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x21 << 3) | 5:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x22 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x22 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* RLA (zp,X) (undoc) */
	case (0x23 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x23 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x23 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x23 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x23 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x23 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x23 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x23 << 3) | 7:
		cpu.FETCH()
		/* BIT zp */
	case (0x24 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x24 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x24 << 3) | 2:
		cpu.m6502bit(cpu.GD())
		cpu.FETCH()
	/* AND zp */
	case (0x25 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x25 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x25 << 3) | 2:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ROL zp */
	case (0x26 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x26 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x26 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x26 << 3) | 3:
		cpu.SD(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x26 << 3) | 4:
		cpu.FETCH()
	/* RLA zp (undoc) */
	case (0x27 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x27 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x27 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x27 << 3) | 3:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x27 << 3) | 4:
		cpu.FETCH()
	/* PLP  */
	case (0x28 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x28 << 3) | 1:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
	case (0x28 << 3) | 2:
		cpu.SA(0x0100 | uint16(cpu.S))
	case (0x28 << 3) | 3:
		cpu.P = (cpu.GD() | m6502BF) & (m6502XF ^ 0xFF)
		cpu.FETCH()
	/* AND # */
	case (0x29 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x29 << 3) | 1:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ROL A  */
	case (0x2A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x2A << 3) | 1:
		cpu.A = cpu.m6502shiftleft(cpu.getCarry(), cpu.A)
		cpu.FETCH()
	/* ANC # (undoc) */
	case (0x2B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x2B << 3) | 1:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		if cpu.A&0x80 != 0 {
			cpu.P |= m6502CF
		} else {
			cpu.P &= m6502CF ^ 0xFF
		}
		cpu.FETCH()
	/* BIT abs */
	case (0x2C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x2C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x2C << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x2C << 3) | 3:
		cpu.m6502bit(cpu.GD())
		cpu.FETCH()
	/* AND abs */
	case (0x2D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x2D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x2D << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x2D << 3) | 3:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ROL abs */
	case (0x2E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x2E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x2E << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x2E << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x2E << 3) | 4:
		cpu.SD(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x2E << 3) | 5:
		cpu.FETCH()
	/* RLA abs (undoc) */
	case (0x2F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x2F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x2F << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x2F << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x2F << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x2F << 3) | 5:
		cpu.FETCH()
	/* BMI # */
	case (0x30 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x30 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x80) != 0x80 {
			cpu.FETCH()
		}
	case (0x30 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0x30 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* AND (zp),Y */
	case (0x31 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x31 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x31 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x31 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x31 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x31 << 3) | 5:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x32 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x32 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* RLA (zp),Y (undoc) */
	case (0x33 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x33 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x33 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x33 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x33 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x33 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x33 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x33 << 3) | 7:
		cpu.FETCH()
	/* NOP zp,X (undoc) */
	case (0x34 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x34 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x34 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x34 << 3) | 3:
		cpu.FETCH()
	/* AND zp,X */
	case (0x35 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x35 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x35 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x35 << 3) | 3:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ROL zp,X */
	case (0x36 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x36 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x36 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x36 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x36 << 3) | 4:
		cpu.SD(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x36 << 3) | 5:
		cpu.FETCH()
	/* RLA zp,X (undoc) */
	case (0x37 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x37 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x37 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x37 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x37 << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x37 << 3) | 5:
		cpu.FETCH()
	/* SEC  */
	case (0x38 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x38 << 3) | 1:
		cpu.P |= 0x1
		cpu.FETCH()
	/* AND abs,Y */
	case (0x39 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x39 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x39 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x39 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x39 << 3) | 4:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0x3A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x3A << 3) | 1:
		cpu.FETCH()
	/* RLA abs,Y (undoc) */
	case (0x3B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x3B << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x3B << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x3B << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x3B << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x3B << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x3B << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0x3C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x3C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x3C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x3C << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x3C << 3) | 4:
		cpu.FETCH()
	/* AND abs,X */
	case (0x3D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x3D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x3D << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x3D << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x3D << 3) | 4:
		cpu.A &= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ROL abs,X */
	case (0x3E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x3E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x3E << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x3E << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x3E << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x3E << 3) | 5:
		cpu.SD(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x3E << 3) | 6:
		cpu.FETCH()
	/* RLA abs,X (undoc) */
	case (0x3F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x3F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x3F << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x3F << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x3F << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x3F << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftleft(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A &= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x3F << 3) | 6:
		cpu.FETCH()
	/* RTI  */
	case (0x40 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x40 << 3) | 1:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
	case (0x40 << 3) | 2:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
	case (0x40 << 3) | 3:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
		cpu.P = (cpu.GD() | m6502BF) & (m6502XF ^ 0xFF)
	case (0x40 << 3) | 4:
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.AD = uint16(cpu.GD())
	case (0x40 << 3) | 5:
		cpu.PC = (uint16(cpu.GD()) << 8) | cpu.AD
		cpu.FETCH()
	/* EOR (zp,X) */
	case (0x41 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x41 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x41 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x41 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x41 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x41 << 3) | 5:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x42 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x42 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* SRE (zp,X) (undoc) */
	case (0x43 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x43 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x43 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x43 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x43 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x43 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x43 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x43 << 3) | 7:
		cpu.FETCH()
		/* NOP zp (undoc) */
	case (0x44 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x44 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x44 << 3) | 2:
		cpu.FETCH()
	/* EOR zp */
	case (0x45 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x45 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x45 << 3) | 2:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LSR zp */
	case (0x46 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x46 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x46 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x46 << 3) | 3:
		cpu.SD(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x46 << 3) | 4:
		cpu.FETCH()
	/* SRE zp (undoc) */
	case (0x47 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x47 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x47 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x47 << 3) | 3:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x47 << 3) | 4:
		cpu.FETCH()
	/* PHA  */
	case (0x48 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x48 << 3) | 1:
		cpu.S--
		cpu.SAD(0x0100|uint16(cpu.S), cpu.A)
		cpu.WR()
	case (0x48 << 3) | 2:
		cpu.FETCH()
	/* EOR # */
	case (0x49 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x49 << 3) | 1:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LSR A  */
	case (0x4A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x4A << 3) | 1:
		cpu.A = cpu.m6502shiftright(0, cpu.A)
		cpu.FETCH()
	/* ASR # (undoc) */
	case (0x4B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x4B << 3) | 1:
		cpu.A &= cpu.GD()
		cpu.A = cpu.m6502shiftright(0, cpu.A)
		cpu.FETCH()
	/* JMP  */
	case (0x4C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x4C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x4C << 3) | 2:
		cpu.PC = (uint16(cpu.GD()) << 8) | cpu.AD
		cpu.FETCH()
	/* EOR abs */
	case (0x4D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x4D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x4D << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x4D << 3) | 3:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LSR abs */
	case (0x4E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x4E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x4E << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x4E << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x4E << 3) | 4:
		cpu.SD(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x4E << 3) | 5:
		cpu.FETCH()
	/* SRE abs (undoc) */
	case (0x4F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x4F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x4F << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x4F << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x4F << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x4F << 3) | 5:
		cpu.FETCH()
	/* BVC # */
	case (0x50 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x50 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x40) != 0x0 {
			cpu.FETCH()
		}
	case (0x50 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0x50 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* EOR (zp),Y */
	case (0x51 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x51 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x51 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x51 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8

		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x51 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x51 << 3) | 5:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x52 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x52 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* SRE (zp),Y (undoc) */
	case (0x53 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x53 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x53 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x53 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x53 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x53 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x53 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x53 << 3) | 7:
		cpu.FETCH()
		/* NOP zp,X (undoc) */
	case (0x54 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x54 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x54 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x54 << 3) | 3:
		cpu.FETCH()
	/* EOR zp,X */
	case (0x55 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x55 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x55 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x55 << 3) | 3:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LSR zp,X */
	case (0x56 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x56 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x56 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x56 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x56 << 3) | 4:
		cpu.SD(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x56 << 3) | 5:
		cpu.FETCH()
	/* SRE zp,X (undoc) */
	case (0x57 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x57 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x57 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x57 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x57 << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x57 << 3) | 5:
		cpu.FETCH()
	/* CLI  */
	case (0x58 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x58 << 3) | 1:
		cpu.P &= 0xFB
		cpu.FETCH()
	/* EOR abs,Y */
	case (0x59 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x59 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x59 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8

		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x59 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x59 << 3) | 4:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0x5A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x5A << 3) | 1:
		cpu.FETCH()
	/* SRE abs,Y (undoc) */
	case (0x5B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x5B << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x5B << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x5B << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x5B << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x5B << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x5B << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0x5C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x5C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x5C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x5C << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x5C << 3) | 4:
		cpu.FETCH()
	/* EOR abs,X */
	case (0x5D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x5D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x5D << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x5D << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x5D << 3) | 4:
		cpu.A ^= cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LSR abs,X */
	case (0x5E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x5E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x5E << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x5E << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x5E << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x5E << 3) | 5:
		cpu.SD(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.WR()
	case (0x5E << 3) | 6:
		cpu.FETCH()
	/* SRE abs,X (undoc) */
	case (0x5F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x5F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x5F << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x5F << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x5F << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x5F << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftright(0, uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.A ^= uint8(cpu.AD)
		cpu.NZ(cpu.A)
		cpu.WR()
	case (0x5F << 3) | 6:
		cpu.FETCH()
	/* RTS  */
	case (0x60 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x60 << 3) | 1:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
	case (0x60 << 3) | 2:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
	case (0x60 << 3) | 3:
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.AD = uint16(cpu.GD())
	case (0x60 << 3) | 4:
		cpu.PC = (uint16(cpu.GD()) << 8) | cpu.AD
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x60 << 3) | 5:
		cpu.FETCH()
	/* ADC (zp,X) */
	case (0x61 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x61 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x61 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x61 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x61 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x61 << 3) | 5:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x62 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x62 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* RRA (zp,X) (undoc) */
	case (0x63 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x63 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x63 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x63 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x63 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x63 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x63 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x63 << 3) | 7:
		cpu.FETCH()
		/* NOP zp (undoc) */
	case (0x64 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x64 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x64 << 3) | 2:
		cpu.FETCH()
	/* ADC zp */
	case (0x65 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x65 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x65 << 3) | 2:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* ROR zp */
	case (0x66 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x66 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x66 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x66 << 3) | 3:
		cpu.SD(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x66 << 3) | 4:
		cpu.FETCH()
	/* RRA zp (undoc) */
	case (0x67 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x67 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0x67 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x67 << 3) | 3:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x67 << 3) | 4:
		cpu.FETCH()
	/* PLA  */
	case (0x68 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x68 << 3) | 1:
		cpu.S++
		cpu.SA(0x0100 | uint16(cpu.S))
		cpu.WR()
	case (0x68 << 3) | 2:
		cpu.SA(0x0100 | uint16(cpu.S))
	case (0x68 << 3) | 3:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ADC # */
	case (0x69 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x69 << 3) | 1:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* RORA  */
	case (0x6A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x6A << 3) | 1:
		cpu.A = cpu.m6502shiftright(cpu.getCarry(), cpu.A)
		cpu.FETCH()
	/* ARR # (undoc) */
	case (0x6B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x6B << 3) | 1:
		cpu.m6502arr(cpu.GD())
		cpu.FETCH()
	/* JMPI  */
	case (0x6C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x6C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x6C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA(cpu.AD)
	case (0x6C << 3) | 3:
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + 1) & 0x00FF))
		cpu.AD = uint16(cpu.GD())
	case (0x6C << 3) | 4:
		cpu.PC = (uint16(cpu.GD()) << 8) | cpu.AD
		cpu.FETCH()
	/* ADC abs */
	case (0x6D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x6D << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x6D << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x6D << 3) | 3:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* ROR abs */
	case (0x6E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x6E << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x6E << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x6E << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x6E << 3) | 4:
		cpu.SD(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x6E << 3) | 5:
		cpu.FETCH()
	/* RRA abs (undoc) */
	case (0x6F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x6F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x6F << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0x6F << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x6F << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x6F << 3) | 5:
		cpu.FETCH()
	/* BVS # */
	case (0x70 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x70 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x40) != 0x40 {
			cpu.FETCH()
		}
	case (0x70 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0x70 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* ADC (zp),Y */
	case (0x71 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x71 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x71 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x71 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x71 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x71 << 3) | 5:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x72 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x72 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* RRA (zp),Y (undoc) */
	case (0x73 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x73 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x73 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x73 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x73 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x73 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x73 << 3) | 6:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x73 << 3) | 7:
		cpu.FETCH()
		/* NOP zp,X (undoc) */
	case (0x74 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x74 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x74 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x74 << 3) | 3:
		cpu.FETCH()
	/* ADC zp,X */
	case (0x75 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x75 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x75 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x75 << 3) | 3:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* ROR zp,X */
	case (0x76 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x76 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x76 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x76 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x76 << 3) | 4:
		cpu.SD(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x76 << 3) | 5:
		cpu.FETCH()
	/* RRA zp,X (undoc) */
	case (0x77 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x77 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x77 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0x77 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x77 << 3) | 4:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x77 << 3) | 5:
		cpu.FETCH()
	/* SEI  */
	case (0x78 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x78 << 3) | 1:
		cpu.P |= 0x4
		cpu.FETCH()
	/* ADC abs,Y */
	case (0x79 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x79 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x79 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0x79 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x79 << 3) | 4:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0x7A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x7A << 3) | 1:
		cpu.FETCH()
	/* RRA abs,Y (undoc) */
	case (0x7B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x7B << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x7B << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x7B << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0x7B << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x7B << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x7B << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0x7C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x7C << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x7C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x7C << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x7C << 3) | 4:
		cpu.FETCH()
	/* ADC abs,X */
	case (0x7D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x7D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x7D << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0x7D << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x7D << 3) | 4:
		cpu.m6502adc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* ROR abs,X */
	case (0x7E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x7E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x7E << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x7E << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x7E << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x7E << 3) | 5:
		cpu.SD(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.WR()
	case (0x7E << 3) | 6:
		cpu.FETCH()
	/* RRA abs,X (undoc) */
	case (0x7F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x7F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x7F << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x7F << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0x7F << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0x7F << 3) | 5:
		cpu.AD = uint16(cpu.m6502shiftright(cpu.getCarry(), uint8(cpu.AD)))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502adc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0x7F << 3) | 6:
		cpu.FETCH()
	/* NOP # (undoc) */
	case (0x80 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x80 << 3) | 1:
		cpu.FETCH()
	/* STA (zp,X) */
	case (0x81 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x81 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x81 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x81 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x81 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x81 << 3) | 5:
		cpu.FETCH()
	/* NOP # (undoc) */
	case (0x82 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x82 << 3) | 1:
		cpu.FETCH()
	/* SAX (zp,X) (undoc) */
	case (0x83 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x83 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x83 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0x83 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x83 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.A & cpu.X)
		cpu.WR()
	case (0x83 << 3) | 5:
		cpu.FETCH()
	/* STY zp */
	case (0x84 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x84 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
		cpu.SD(cpu.Y)
		cpu.WR()
	case (0x84 << 3) | 2:
		cpu.FETCH()
	/* STA zp */
	case (0x85 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x85 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x85 << 3) | 2:
		cpu.FETCH()
	/* STX zp */
	case (0x86 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x86 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
		cpu.SD(cpu.X)
		cpu.WR()
	case (0x86 << 3) | 2:
		cpu.FETCH()
	/* SAX zp (undoc) */
	case (0x87 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x87 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
		cpu.SD(cpu.A & cpu.X)
		cpu.WR()
	case (0x87 << 3) | 2:
		cpu.FETCH()
	/* DEY  */
	case (0x88 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x88 << 3) | 1:
		cpu.Y--
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* NOP # (undoc) */
	case (0x89 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x89 << 3) | 1:
		cpu.FETCH()
	/* TXA  */
	case (0x8A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x8A << 3) | 1:
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* ANE # (undoc) */
	case (0x8B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x8B << 3) | 1:
		cpu.A = (cpu.A | 0xEE) & cpu.X & cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* STY abs */
	case (0x8C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x8C << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x8C << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.Y)
		cpu.WR()
	case (0x8C << 3) | 3:
		cpu.FETCH()
	/* STA abs */
	case (0x8D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x8D << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x8D << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x8D << 3) | 3:
		cpu.FETCH()
	/* STX abs */
	case (0x8E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x8E << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x8E << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.X)
		cpu.WR()
	case (0x8E << 3) | 3:
		cpu.FETCH()
	/* SAX abs (undoc) */
	case (0x8F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x8F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x8F << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
		cpu.SD(cpu.A & cpu.X)
		cpu.WR()
	case (0x8F << 3) | 3:
		cpu.FETCH()
	/* BCC # */
	case (0x90 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x90 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x1) != 0x0 {
			cpu.FETCH()
		}
	case (0x90 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0x90 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* STA (zp),Y */
	case (0x91 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x91 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x91 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x91 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x91 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x91 << 3) | 5:
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0x92 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x92 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* SHA (zp),Y (undoc) */
	case (0x93 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x93 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x93 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0x93 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x93 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.SD(cpu.A & cpu.X & (uint8((cpu.GA() >> 8) + 1)))
		cpu.WR()
	case (0x93 << 3) | 5:
		cpu.FETCH()
	/* STY zp,X */
	case (0x94 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x94 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x94 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
		cpu.SD(cpu.Y)
		cpu.WR()
	case (0x94 << 3) | 3:
		cpu.FETCH()
	/* STA zp,X */
	case (0x95 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x95 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x95 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x95 << 3) | 3:
		cpu.FETCH()
	/* STX zp,Y */
	case (0x96 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x96 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x96 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.Y)) & 0x00FF)
		cpu.SD(cpu.X)
		cpu.WR()
	case (0x96 << 3) | 3:
		cpu.FETCH()
	/* SAX zp,Y (undoc) */
	case (0x97 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x97 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0x97 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.Y)) & 0x00FF)
		cpu.SD(cpu.A & cpu.X)
		cpu.WR()
	case (0x97 << 3) | 3:
		cpu.FETCH()
	/* TYA  */
	case (0x98 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x98 << 3) | 1:
		cpu.A = cpu.Y
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* STA abs,Y */
	case (0x99 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x99 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x99 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x99 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x99 << 3) | 4:
		cpu.FETCH()
	/* TXS  */
	case (0x9A << 3) | 0:
		cpu.SA(cpu.PC)
	case (0x9A << 3) | 1:
		cpu.S = cpu.X
		cpu.FETCH()
	/* SHS abs,Y (undoc) */
	case (0x9B << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x9B << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x9B << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x9B << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.S = cpu.A & cpu.X
		cpu.SD(cpu.S & (uint8(((cpu.GA() >> 8) + 1))))
		cpu.WR()
	case (0x9B << 3) | 4:
		cpu.FETCH()
	/* SHY abs,X (undoc) */
	case (0x9C << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x9C << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x9C << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x9C << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
		cpu.SD(cpu.Y & (uint8((cpu.GA() >> 8) + 1)))
		cpu.WR()
	case (0x9C << 3) | 4:
		cpu.FETCH()
	/* STA abs,X */
	case (0x9D << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x9D << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x9D << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0x9D << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
		cpu.SD(cpu.A)
		cpu.WR()
	case (0x9D << 3) | 4:
		cpu.FETCH()
	/* SHX abs,Y (undoc) */
	case (0x9E << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x9E << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x9E << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x9E << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.SD(cpu.X & (uint8((cpu.GA() >> 8) + 1)))
		cpu.WR()
	case (0x9E << 3) | 4:
		cpu.FETCH()
	/* SHA abs,Y (undoc) */
	case (0x9F << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0x9F << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0x9F << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0x9F << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
		cpu.SD(cpu.A & cpu.X & (uint8((cpu.GA() >> 8) + 1)))
		cpu.WR()
	case (0x9F << 3) | 4:
		cpu.FETCH()
	/* LDY # */
	case (0xA0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA0 << 3) | 1:
		cpu.Y = cpu.GD()
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA (zp,X) */
	case (0xA1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xA1 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xA1 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xA1 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xA1 << 3) | 5:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDX # */
	case (0xA2 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA2 << 3) | 1:
		cpu.X = cpu.GD()
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAX (zp,X) (undoc) */
	case (0xA3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xA3 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xA3 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xA3 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xA3 << 3) | 5:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDY zp */
	case (0xA4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA4 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xA4 << 3) | 2:
		cpu.Y = cpu.GD()
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA zp */
	case (0xA5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA5 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xA5 << 3) | 2:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDX zp */
	case (0xA6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA6 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xA6 << 3) | 2:
		cpu.X = cpu.GD()
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAX zp (undoc) */
	case (0xA7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA7 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xA7 << 3) | 2:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* TAY  */
	case (0xA8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xA8 << 3) | 1:
		cpu.Y = cpu.A
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA # */
	case (0xA9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xA9 << 3) | 1:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* TAX  */
	case (0xAA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xAA << 3) | 1:
		cpu.X = cpu.A
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LXA # (undoc) */
	case (0xAB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xAB << 3) | 1:
		cpu.X = (cpu.A | 0xEE) & cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDY abs */
	case (0xAC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xAC << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xAC << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xAC << 3) | 3:
		cpu.Y = cpu.GD()
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA abs */
	case (0xAD << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xAD << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xAD << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xAD << 3) | 3:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDX abs */
	case (0xAE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xAE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xAE << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xAE << 3) | 3:
		cpu.X = cpu.GD()
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAX abs (undoc) */
	case (0xAF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xAF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xAF << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xAF << 3) | 3:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* BCS # */
	case (0xB0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB0 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x1) != 0x1 {
			cpu.FETCH()
		}
	case (0xB0 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0xB0 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* LDA (zp),Y */
	case (0xB1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB1 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xB1 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8

		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xB1 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xB1 << 3) | 5:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0xB2 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xB2 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* LAX (zp),Y (undoc) */
	case (0xB3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB3 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xB3 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xB3 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xB3 << 3) | 5:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDY zp,X */
	case (0xB4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB4 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB4 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xB4 << 3) | 3:
		cpu.Y = cpu.GD()
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA zp,X */
	case (0xB5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB5 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB5 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xB5 << 3) | 3:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDX zp,Y */
	case (0xB6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB6 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB6 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.Y)) & 0x00FF)
	case (0xB6 << 3) | 3:
		cpu.X = cpu.GD()
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAX zp,Y (undoc) */
	case (0xB7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB7 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xB7 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.Y)) & 0x00FF)
	case (0xB7 << 3) | 3:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* CLV  */
	case (0xB8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xB8 << 3) | 1:
		cpu.P &= 0xBF
		cpu.FETCH()
	/* LDA abs,Y */
	case (0xB9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xB9 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xB9 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xB9 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xB9 << 3) | 4:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* TSX  */
	case (0xBA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xBA << 3) | 1:
		cpu.X = cpu.S
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAS abs,Y (undoc) */
	case (0xBB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xBB << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xBB << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xBB << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xBB << 3) | 4:
		cpu.S = cpu.GD() & cpu.S
		cpu.X = cpu.S
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDY abs,X */
	case (0xBC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xBC << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xBC << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xBC << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xBC << 3) | 4:
		cpu.Y = cpu.GD()
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* LDA abs,X */
	case (0xBD << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xBD << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xBD << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xBD << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xBD << 3) | 4:
		cpu.A = cpu.GD()
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* LDX abs,Y */
	case (0xBE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xBE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xBE << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xBE << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xBE << 3) | 4:
		cpu.X = cpu.GD()
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* LAX abs,Y (undoc) */
	case (0xBF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xBF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xBF << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xBF << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xBF << 3) | 4:
		cpu.X = cpu.GD()
		cpu.A = cpu.X
		cpu.NZ(cpu.A)
		cpu.FETCH()
	/* CPY # */
	case (0xC0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC0 << 3) | 1:
		cpu.m6502cmp(cpu.Y, cpu.GD())
		cpu.FETCH()
	/* CMP (zp,X) */
	case (0xC1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xC1 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xC1 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xC1 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xC1 << 3) | 5:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* NOP # (undoc) */
	case (0xC2 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC2 << 3) | 1:
		cpu.FETCH()
	/* DCP (zp,X) (undoc) */
	case (0xC3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xC3 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xC3 << 3) | 3:

		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xC3 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xC3 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xC3 << 3) | 6:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xC3 << 3) | 7:
		cpu.FETCH()
		/* CPY zp */
	case (0xC4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC4 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xC4 << 3) | 2:
		cpu.m6502cmp(cpu.Y, cpu.GD())
		cpu.FETCH()
	/* CMP zp */
	case (0xC5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC5 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xC5 << 3) | 2:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* DEC zp */
	case (0xC6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC6 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xC6 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xC6 << 3) | 3:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xC6 << 3) | 4:
		cpu.FETCH()
	/* DCP zp (undoc) */
	case (0xC7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC7 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xC7 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xC7 << 3) | 3:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xC7 << 3) | 4:
		cpu.FETCH()
	/* INY  */
	case (0xC8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xC8 << 3) | 1:
		cpu.Y++
		cpu.NZ(cpu.Y)
		cpu.FETCH()
	/* CMP # */
	case (0xC9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xC9 << 3) | 1:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* DEX  */
	case (0xCA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xCA << 3) | 1:
		cpu.X--
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* SBX # (undoc) */
	case (0xCB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xCB << 3) | 1:
		cpu.m6502sbx(cpu.GD())
		cpu.FETCH()
	/* CPY abs */
	case (0xCC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xCC << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xCC << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xCC << 3) | 3:
		cpu.m6502cmp(cpu.Y, cpu.GD())
		cpu.FETCH()
	/* CMP abs */
	case (0xCD << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xCD << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xCD << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xCD << 3) | 3:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* DEC abs */
	case (0xCE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xCE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xCE << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xCE << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xCE << 3) | 4:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xCE << 3) | 5:
		cpu.FETCH()
	/* DCP abs (undoc) */
	case (0xCF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xCF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xCF << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xCF << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xCF << 3) | 4:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xCF << 3) | 5:
		cpu.FETCH()
	/* BNE # */
	case (0xD0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD0 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x2) != 0x0 {
			cpu.FETCH()
		}
	case (0xD0 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0xD0 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* CMP (zp),Y */
	case (0xD1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD1 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xD1 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xD1 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xD1 << 3) | 5:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0xD2 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xD2 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* DCP (zp),Y (undoc) */
	case (0xD3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD3 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xD3 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0xD3 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xD3 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xD3 << 3) | 6:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xD3 << 3) | 7:
		cpu.FETCH()
		/* NOP zp,X (undoc) */
	case (0xD4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD4 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD4 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xD4 << 3) | 3:
		cpu.FETCH()
	/* CMP zp,X */
	case (0xD5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD5 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD5 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xD5 << 3) | 3:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* DEC zp,X */
	case (0xD6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD6 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD6 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xD6 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xD6 << 3) | 4:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xD6 << 3) | 5:
		cpu.FETCH()
	/* DCP zp,X (undoc) */
	case (0xD7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD7 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xD7 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xD7 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xD7 << 3) | 4:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xD7 << 3) | 5:
		cpu.FETCH()
	/* CLD  */
	case (0xD8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xD8 << 3) | 1:
		cpu.P &= 0xF7
		cpu.FETCH()
	/* CMP abs,Y */
	case (0xD9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xD9 << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xD9 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xD9 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xD9 << 3) | 4:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0xDA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xDA << 3) | 1:
		cpu.FETCH()
	/* DCP abs,Y (undoc) */
	case (0xDB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xDB << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xDB << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0xDB << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xDB << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xDB << 3) | 5:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xDB << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0xDC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xDC << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xDC << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xDC << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xDC << 3) | 4:
		cpu.FETCH()
	/* CMP abs,X */
	case (0xDD << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xDD << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xDD << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xDD << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xDD << 3) | 4:
		cpu.m6502cmp(cpu.A, cpu.GD())
		cpu.FETCH()
	/* DEC abs,X */
	case (0xDE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xDE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xDE << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0xDE << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xDE << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xDE << 3) | 5:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xDE << 3) | 6:
		cpu.FETCH()
	/* DCP abs,X (undoc) */
	case (0xDF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xDF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xDF << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0xDF << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xDF << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xDF << 3) | 5:
		cpu.AD--
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.m6502cmp(cpu.A, uint8(cpu.AD))
		cpu.WR()
	case (0xDF << 3) | 6:
		cpu.FETCH()
	/* CPX # */
	case (0xE0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE0 << 3) | 1:
		cpu.m6502cmp(cpu.X, cpu.GD())
		cpu.FETCH()
	/* SBC (zp,X) */
	case (0xE1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xE1 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xE1 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xE1 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xE1 << 3) | 5:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* NOP # (undoc) */
	case (0xE2 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE2 << 3) | 1:
		cpu.FETCH()
	/* ISB (zp,X) (undoc) */
	case (0xE3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xE3 << 3) | 2:
		cpu.AD = (cpu.AD + uint16(cpu.X)) & 0xFF
		cpu.SA(cpu.AD)
	case (0xE3 << 3) | 3:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xE3 << 3) | 4:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xE3 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xE3 << 3) | 6:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xE3 << 3) | 7:
		cpu.FETCH()
	/* CPX zp */
	case (0xE4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE4 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xE4 << 3) | 2:
		cpu.m6502cmp(cpu.X, cpu.GD())
		cpu.FETCH()
	/* SBC zp */
	case (0xE5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE5 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xE5 << 3) | 2:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* INC zp */
	case (0xE6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE6 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xE6 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xE6 << 3) | 3:
		cpu.AD++
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xE6 << 3) | 4:
		cpu.FETCH()
	/* ISB zp (undoc) */
	case (0xE7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE7 << 3) | 1:
		cpu.SA(uint16(cpu.GD()))
	case (0xE7 << 3) | 2:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xE7 << 3) | 3:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xE7 << 3) | 4:
		cpu.FETCH()
	/* INX  */
	case (0xE8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xE8 << 3) | 1:
		cpu.X++
		cpu.NZ(cpu.X)
		cpu.FETCH()
	/* SBC # */
	case (0xE9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xE9 << 3) | 1:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* NOP  */
	case (0xEA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xEA << 3) | 1:
		cpu.FETCH()
	/* SBC # (undoc) */
	case (0xEB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xEB << 3) | 1:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* CPX abs */
	case (0xEC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xEC << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xEC << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xEC << 3) | 3:
		cpu.m6502cmp(cpu.X, cpu.GD())
		cpu.FETCH()
	/* SBC abs */
	case (0xED << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xED << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xED << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xED << 3) | 3:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* INC abs */
	case (0xEE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xEE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xEE << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xEE << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xEE << 3) | 4:
		cpu.AD++
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xEE << 3) | 5:
		cpu.FETCH()
	/* ISB abs (undoc) */
	case (0xEF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xEF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xEF << 3) | 2:
		cpu.SA((uint16(cpu.GD()) << 8) | cpu.AD)
	case (0xEF << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xEF << 3) | 4:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xEF << 3) | 5:
		cpu.FETCH()
	/* BEQ # */
	case (0xF0 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF0 << 3) | 1:
		cpu.SA(cpu.PC)
		cpu.AD = cpu.PC + uint16(cpu.GD())
		if (cpu.P & 0x2) != 0x2 {
			cpu.FETCH()
		}
	case (0xF0 << 3) | 2:
		cpu.SA((cpu.PC & 0xFF00) | (cpu.AD & 0x00FF))
		if (cpu.AD & 0xFF00) == (cpu.PC & 0xFF00) {
			cpu.PC = cpu.AD
			cpu.irqpip >>= 1
			cpu.nmipip >>= 1
			cpu.FETCH()
		}
	case (0xF0 << 3) | 3:
		cpu.PC = cpu.AD
		cpu.FETCH()
	/* SBC (zp),Y */
	case (0xF1 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF1 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF1 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xF1 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xF1 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xF1 << 3) | 5:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* JAM INVALID (undoc) */
	case (0xF2 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xF2 << 3) | 1:
		cpu.SAD(0xFFFF, 0xFF)
		cpu.IR--
	/* ISB (zp),Y (undoc) */
	case (0xF3 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF3 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF3 << 3) | 2:
		cpu.SA((cpu.AD + 1) & 0xFF)
		cpu.AD = uint16(cpu.GD())
	case (0xF3 << 3) | 3:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0xF3 << 3) | 4:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xF3 << 3) | 5:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xF3 << 3) | 6:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xF3 << 3) | 7:
		cpu.FETCH()
		/* NOP zp,X (undoc) */
	case (0xF4 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF4 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF4 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xF4 << 3) | 3:
		cpu.FETCH()
	/* SBC zp,X */
	case (0xF5 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF5 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF5 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xF5 << 3) | 3:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* INC zp,X */
	case (0xF6 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF6 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF6 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xF6 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xF6 << 3) | 4:
		cpu.AD++
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xF6 << 3) | 5:
		cpu.FETCH()
	/* ISB zp,X (undoc) */
	case (0xF7 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF7 << 3) | 1:
		cpu.AD = uint16(cpu.GD())
		cpu.SA(cpu.AD)
	case (0xF7 << 3) | 2:
		cpu.SA((cpu.AD + uint16(cpu.X)) & 0x00FF)
	case (0xF7 << 3) | 3:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xF7 << 3) | 4:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xF7 << 3) | 5:
		cpu.FETCH()
	/* SED  */
	case (0xF8 << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xF8 << 3) | 1:
		cpu.P |= 0x8
		cpu.FETCH()
	/* SBC abs,Y */
	case (0xF9 << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xF9 << 3) | 1:
		cpu.PC++

		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xF9 << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8

		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.Y)) >> 8) ^ 0xFFFF) & 1
	case (0xF9 << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xF9 << 3) | 4:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* NOP  (undoc) */
	case (0xFA << 3) | 0:
		cpu.SA(cpu.PC)
	case (0xFA << 3) | 1:
		cpu.FETCH()
	/* ISB abs,Y (undoc) */
	case (0xFB << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xFB << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xFB << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.Y)) & 0xFF))
	case (0xFB << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.Y))
	case (0xFB << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xFB << 3) | 5:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xFB << 3) | 6:
		cpu.FETCH()
	/* NOP abs,X (undoc) */
	case (0xFC << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xFC << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xFC << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xFC << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xFC << 3) | 4:
		cpu.FETCH()
	/* SBC abs,X */
	case (0xFD << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xFD << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xFD << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
		cpu.IR += ((cpu.AD >> 8) - ((cpu.AD + uint16(cpu.X)) >> 8) ^ 0xFFFF) & 1
	case (0xFD << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xFD << 3) | 4:
		cpu.m6502sbc(cpu.getCarry(), cpu.GD())
		cpu.FETCH()
	/* INC abs,X */
	case (0xFE << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xFE << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xFE << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0xFE << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xFE << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xFE << 3) | 5:
		cpu.AD++
		cpu.NZ(uint8(cpu.AD))
		cpu.SD(uint8(cpu.AD))
		cpu.WR()
	case (0xFE << 3) | 6:
		cpu.FETCH()
	/* ISB abs,X (undoc) */
	case (0xFF << 3) | 0:
		cpu.PC++
		cpu.SA(cpu.PC)
	case (0xFF << 3) | 1:
		cpu.PC++
		cpu.SA(cpu.PC)
		cpu.AD = uint16(cpu.GD())
	case (0xFF << 3) | 2:
		cpu.AD |= uint16(cpu.GD()) << 8
		cpu.SA((cpu.AD & 0xFF00) | ((cpu.AD + uint16(cpu.X)) & 0xFF))
	case (0xFF << 3) | 3:
		cpu.SA(cpu.AD + uint16(cpu.X))
	case (0xFF << 3) | 4:
		cpu.AD = uint16(cpu.GD())
		cpu.WR()
	case (0xFF << 3) | 5:
		cpu.AD++
		cpu.SD(uint8(cpu.AD))
		cpu.m6502sbc(cpu.getCarry(), uint8(cpu.AD))
		cpu.WR()
	case (0xFF << 3) | 6:
		cpu.FETCH()
	}
	cpu.IR++
	//m6510SETPORT(pins, cpu.iopins)
	cpu.Pins = pins
	cpu.irqpip <<= 1
	cpu.nmipip <<= 1

	return pins
}

/* register access functions */
func (cpu *M6502a) m6502SetA(v uint8)   { cpu.A = v }
func (cpu *M6502a) m6502SetX(v uint8)   { cpu.X = v }
func (cpu *M6502a) m6502SetY(v uint8)   { cpu.Y = v }
func (cpu *M6502a) m6502SetS(v uint8)   { cpu.S = v }
func (cpu *M6502a) m6502SetP(v uint8)   { cpu.P = v }
func (cpu *M6502a) m6502SetPC(v uint16) { cpu.PC = v }
func (cpu *M6502a) m6502A() uint8       { return cpu.A }
func (cpu *M6502a) m6502X() uint8       { return cpu.X }
func (cpu *M6502a) m6502Y() uint8       { return cpu.Y }
func (cpu *M6502a) m6502S() uint8       { return cpu.S }
func (cpu *M6502a) m6502P() uint8       { return cpu.P }
func (cpu *M6502a) m6502PC() uint16     { return cpu.PC }

/* helper macros and functions for code-generated instruction decoder */

func (cpu *M6502a) setZero(flag bool) {

	cpu.P &= m6502ZF ^ 0xFF // clear

	if flag {
		cpu.P |= m6502ZF
	}

}

func (cpu *M6502a) setNegative(flag bool) {

	cpu.P &= m6502NF ^ 0xFF // clear

	if flag {
		cpu.P |= m6502NF
	}

}

func (cpu *M6502a) setOverflow(flag bool) {

	cpu.P &= m6502VF ^ 0xFF // clear

	if flag {
		cpu.P |= m6502VF
	}

}

func (cpu *M6502a) setCarry(flag bool) {

	cpu.P &= m6502CF ^ 0xFF // clear

	if flag {
		cpu.P |= m6502CF
	}

}

func (cpu *M6502a) getCarry() uint8 {
	var carry uint8
	carry = 0

	if cpu.P&m6502CF != 0 {
		carry = 1
	}

	return carry
}

func (cpu *M6502a) NZ(value uint8) {
	cpu.P &= (m6502NF | m6502ZF) ^ 0xFF // clear

	if value == 0 {
		cpu.P |= m6502ZF
	}
	cpu.P |= value & m6502NF

}

func (cpu *M6502a) m6502adc(carryIn uint8, value uint8) {
	var carryLowByte, lowByte, highByte uint8
	var result int16
	carryLowByte = 0
	result = int16(cpu.A) + int16(value) + int16(carryIn)

	if cpu.bcdenabled && (cpu.P&m6502DF) != 0 {
		/* decimal mode (credit goes to MAME) */
		lowByte = (cpu.A & 0x0F) + (value & 0x0F) + carryIn

		if lowByte > 9 {
			lowByte += 6
			carryLowByte = 1
		}

		highByte = (cpu.A >> 4) + (value >> 4) + carryLowByte

		if highByte > 9 {
			highByte += 6
		}

		cpu.A = (lowByte & 0x0F) | ((highByte << 4) & 0xF0)

		cpu.setCarry(result > 0x99)
		cpu.setZero(cpu.A == 0)                      // zero non consistent in BCD
		cpu.setNegative(cpu.A > 0x7F)                // negative  non consistent in BCD
		cpu.setOverflow(result > 0x99 || result < 0) // overflow  non consistent in BCD

	} else {
		/* default mode */

		cpu.setCarry(result > 0xFF)
		cpu.setOverflow((cpu.A^uint8(result))&(value^uint8(result))&0x80 != 0)

		cpu.A = uint8(result)
		cpu.setZero(cpu.A == 0)
		cpu.setNegative(cpu.A > 0x7F)
	}
}

func (cpu *M6502a) m6502sbc(carryIn uint8, value uint8) {
	var borrow, lowByte, highByte uint8
	var result int16
	borrow = 0
	carryIn = carryIn ^ 0x01
	result = int16(cpu.A) - int16(value) - int16(carryIn)
	cpu.setCarry(result >= 0)

	if cpu.bcdenabled && (cpu.P&m6502DF) != 0 {
		lowByte = (cpu.A & 0x0F) - (value & 0x0F) - carryIn
		if lowByte < 0 {
			lowByte -= 6
			borrow = 1
		}

		highByte = (cpu.A >> 4) - (value >> 4) - borrow

		cpu.A = (highByte << 4) | (lowByte & 0x0F)

		cpu.setOverflow(result > 0x99 || result < 0) // overflow  non consistent in BCD

	} else {
		/* default mode */

		cpu.A = uint8(result)

		cpu.setOverflow(result > 127 || result < -128)

	}

	cpu.setZero(cpu.A == 0)
	cpu.setNegative(cpu.A > 0x7F)

}

func (cpu *M6502a) m6502cmp(register uint8, value uint8) {
	var result uint8
	result = register - value

	cpu.setCarry(register >= value)
	cpu.setZero(result == 0)
	cpu.setNegative(result > 0x7F)

}

func (cpu *M6502a) m6502shiftleft(bitIn uint8, value uint8) uint8 { // ASL bitin = 0 and ROL bitIn = carry
	var result uint8
	cpu.setCarry(value&0x80 != 0)

	result = ((value<<1)&0xFE + bitIn)
	cpu.setNegative(result > 0x7F)
	cpu.setZero(result == 0)

	return result
}

func (cpu *M6502a) m6502shiftright(bitIn uint8, value uint8) uint8 { // LSR bitIn = 0 and ROR bitIn = carry
	var result uint8

	cpu.setCarry(value&0x01 != 0)

	result = (value >> 1) & 0x7F
	result |= bitIn << 4

	cpu.setZero(result == 0)
	cpu.setNegative(result > 0x7F)

	return result
}

func (cpu *M6502a) m6502bit(value uint8) {
	var result uint8
	result = cpu.A & value

	cpu.setOverflow(value&0x40 != 0)
	cpu.setNegative(result > 0x7F)
	cpu.setZero(result == 0)

}

func (cpu *M6502a) m6502arr(value uint8) {
	var result, carry uint8
	carry = cpu.getCarry()
	result = (cpu.A & value) >> 1
	if cpu.bcdenabled && (cpu.P&m6502DF) != 0 {

		if carry == 1 {
			result |= 0x80
		}
		cpu.setNegative(result > 0x7F)
		cpu.setZero(result == 0)

		cpu.setOverflow((result^cpu.A)&0x40 != 0)

		if (cpu.A & 0x0F) >= 5 {
			result = ((result + 6) & 0x0F) | (result & 0xF0)
		}

		if (cpu.A & 0xF0) >= 0x50 {
			result += 0x60
			cpu.setCarry(true)
		}

	} else {
		if carry == 1 {
			result |= 0x80
		}

		cpu.setNegative(result > 0x7F)
		cpu.setZero(result == 0)

		cpu.setCarry(result&0x40 != 0)

		cpu.setOverflow((result&0x40)^((result&0x20)<<1) != 0)
	}

	cpu.A = result

}

/*
undocumented SBX instruction:

	AND X register with accumulator and store result in X register, then
	subtract byte from X register (without borrow) where the
	subtract works like a CMP instruction
*/
func (cpu *M6502a) m6502sbx(value uint8) {
	var result uint8
	result = (cpu.A & cpu.X) - value

	cpu.setCarry(result > 127)

	cpu.X = result
	cpu.setNegative(cpu.X > 0x7F)
	cpu.setZero(cpu.X == 0)
}
