/* Package wozultima/C600G/memory provides virtual memory for IIe (64k/128k)

	memory.go

	C600G is an accurate Apple IIe emulator writting in Golang
  	WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
  	(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License
*/

// Package memory provides a mappable 16-bit addressable 8-bit data bus
// Different Memory backends can be attached at different base addresses.
package memory

import (
	"encoding/hex"
	"fmt"
)

/*
// Memory is a general interface for reading and writing bytes to and from
// 16-bit addresses.
type Memory interface {
	Shutdown()
	Read(uint16) byte
	Write(uint16, byte)
	Size() int
}

*/
// MEM  structure of memory inside bus
type Mem struct {
	Name string
	Size int // bytes
	Data []byte
}

// Shutdown is part of the Memory interface, but takes no action for Mem
func (m *Mem) Shutdown() {
}

// NewMem create a block of new Ram (0 to size -1)
func NewMem(name string, size int) (*Mem, error) {
	data := make([]byte, size)
	for i := range data {
		data[i] = 0xA0
	} // value 0xA0 in memory when start Apple IIe
	return &Mem{Name: name, Size: size, Data: data}, nil
}

func (m *Mem) String() string {
	return fmt.Sprintf("Mem[%s:%dk:%s..%s]",
		m.Name,
		m.Size/1024,
		hex.EncodeToString(m.Data[0:2]),
		hex.EncodeToString(m.Data[len(m.Data)-2:]))
}

// Read a byte from a 16-bit address.
func (m *Mem) ReadMemoryByte(address uint16) byte { // 3
	return m.Data[address]
}

// Write a byte to a 16-bit address.
func (m *Mem) WriteMemoryByte(address uint16, value byte) {
	m.Data[address] = value
}
