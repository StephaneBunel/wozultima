GOMODULE = $(shell head -1 go.mod | cut -d" " -f2)
ROOT = $(realpath $(CURDIR))
BIN = $(ROOT)/bin

SOURCES = go.mod $(shell find "$(ROOT)" \
	 -type f \
	 -name "*.go" \
	 -or -name "*.otf" \
	 -or -name "*.png" \
	 -or -name "Makefile")

GO = go
# LDFLAGS = -s -w ... to remove debug/symbols
LDFLAGS =
GCFLAGS =

$(BIN)/wozultima: $(SOURCES)
	@CGO_ENABLED=1 $(GO) build -v \
	-ldflags "$(LDFLAGS)" \
	-gcflags "$(GCFLAGS)" \
	-o $(BIN)/wozultima $(GOMODULE)/cmd/wozultima

.phony: run
run: $(BIN)/wozultima
	@$(BIN)/wozultima

.phony: force
force: clean $(BIN)/wozultima

.phony: clean
clean:
	@rm -rf $(BIN)

